from stdafx import *

import game_framework, TSRoomScene, TSInGameScene
from Sound.Sound import *

name = "MainScene"
TitleImage = None
PressButtonImage = None
logo_time = 0.0
Sound = None

def enter():
    global TitleImage
    global PressButtonImage
    global Sound
    open_canvas(WIDTH,HEIGHT, True)
    if (TitleImage == None ): TitleImage = load_image('TSData/MS_title.png')
    if (Sound == None): Sound = MainSound()

def exit():
    global TitleImage
    global PressButtonImage

    del TitleImage
    del PressButtonImage
    close_canvas()

def update():
    global logo_time

    logo_time += 0.01

def draw():
    global logo_time
    global TitleImage
    global PressButtonImage
    clear_canvas()
    TitleImage.draw(WIDTH/2, HEIGHT/2)
    if (logo_time < 0.5):
        PressButtonImage = load_image('TSData/MS_pressbutton.png')
        PressButtonImage.draw(WIDTH / 2, HEIGHT * (1/5))
    elif (logo_time > 1.0):
        del (PressButtonImage)
        logo_time = 0
    update_canvas()

def handle_events():
    global Sound
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_SPACE:
            game_framework.push_state(TSInGameScene)
            del Sound

def pause() : pass

def resume() : pass