from stdafx import *
import game_framework, TSInGameScene

name = 'RoomScene'
image = None

def enter():
    global image
    #image = load_image('TSData/title.png')

def exit():
    global image
    del (image)

def handle_events():
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        else:
            if (event.type , event.key ) == (SDL_KEYDOWN , SDLK_ESCAPE):
                game_framework.quit()
            elif (event.type, event.key) == (SDL_KEYDOWN, SDLK_SPACE):
                game_framework.change_state(TSInGameScene)
            elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
                game_framework.quit()


def draw():
    clear_canvas()
    #image.draw(400, 300)
    update_canvas()

def update() : pass
