from stdafx import *

#########################################################################################
##
class UI:
    def __init__(selfs): pass

class BombUI(UI):
    Image = None
    def __init__(self):
        self.x, self.y = 15, 15
        self.w, self.h = BOMB_UI_WIDTH, BOMB_UI_HEIGHT
        if (BombUI.Image == None):
            BombUI.Image = load_image('TSData/BombUi.png')

    def Draw(self):
        BombUI.Image.clip_draw_to_origin(0, 0, 775, 800, self.x, self.y, self.w, self.h)

    def Update(self):
        pass

class ScoreUI(UI):
    Image = None
    def __init__(self, x):
        self.x, self.y = x, HEIGHT - 50
        self.w, self.h = 30, 25
        self.num = 810
        if (ScoreUI.Image == None):
            ScoreUI.Image = load_image('TSData/numbers.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.num

    def Draw(self):
        ScoreUI.Image.clip_draw_to_origin(self.num, 0, 90, 90, self.x, self.y, self.w, self.h)

    def Update(self, idx , score):
        if (score != None):
            #print("score", score)
            if (score == 0):
                self.num = 810
            else:
                self.num = ((score - 1) * 90)

class WeaponLVUI(UI):
    def __init__(self):
       self.LV1_Image = load_image('TSData/WeaponLV1.png')
       self.LV2_Image = load_image('TSData/WeaponLV2.png')
       self.LV3_Image = load_image('TSData/WeaponLV3.png')
       self.LV = 0
    def __del__(self):
        del self.LV1_Image
        del self.LV2_Image
        del self.LV3_Image
        del self.LV

    def Draw(self):
        if (self.LV == 1): self.LV1_Image.clip_draw_to_origin(0, 0, 235, 60, 520, 20, 150, 50)
        elif (self.LV == 2): self.LV2_Image.clip_draw_to_origin(0, 0, 235, 60, 520, 20, 150, 50)
        elif (self.LV == 3): self.LV3_Image.clip_draw_to_origin(0, 0, 235, 60, 520, 20, 150, 50)

    def Update(self, weaponLV):
        self.LV = weaponLV

#UI들을 관리하는 리스트 클래스이다.
class UIList:
    def __init__(self):
        #현재 UIList는 BombUI와 ScoreUI를 관리한다.
        self.Bomb = BombUI()

        self.Weapon = WeaponLVUI()

        self.ScoreList = []
        #scoreidx에 따라 얼마만큼 떨어져서 출력되야하는지 정해진다
        for scoreidx in range(0,10):
            self.ScoreList.append(ScoreUI(25 + (scoreidx * 25)))
            #self.ScoreList.append(ScoreUI(250 - (scoreidx * 25)))

        self.tmpscore = 0
        self.score = 0
        self.remainder0, self.remainder1, self.remainder2, self.remainder3 = 0,0,0,0

    def __del__(self):
        if (self.ScoreList == None): del self.ScoreList
        if (self.Bomb == None): del self.Bomb

        del self.remainder0, self.remainder1, self.remainder2, self.remainder3
        del self.tmpscore, self.score

    def Draw(self):

        #self.Bomb.Draw()

        self.Weapon.Draw()

        for ui in self.ScoreList:
            ui.Draw()

    #나중에 여기 살펴보자
    def Delete(self):
        pass

    def CheckScore(self, idx, score):
        # 몇번째자리에 어떤 숫자가 표현되어야하는지 계산한다.
        # 방법은 점수(score)를 가장 높은 자리수부터 먼저 나누고 그 몫을 ui로 표현하고
        # 나머지는 다음 자리수로 나누고 또 그 몫을 ui로 표현하는 방법을 계속해서 마지막 한자리수까지 표현한다.

        # 한자리 숫자일때
        if (score < 10):
            if (idx == 9): return score // 1

        # 10자리일때
        if (score >= 10 and score <= 99):
            if (idx == 8):
                self.remainder0 = score % 10
                return score // 10
            if (idx == 9):
                return self.remainder0 // 1

        # 100자리일때
        if (score >= 100 and score <= 999):
            if (idx == 7):
                self.remainder0 = score % 100
                return score // 100
            if (idx == 8):
                self.remainder1 = self.remainder0 % 10
                return self.remainder0 // 10
            if (idx == 9):
                return self.remainder1 // 1

        # 1000자리일때
        if (score >= 1000 and score <= 9999):
            if (idx == 6):
                self.remainder0 = score % 1000
                return score // 1000
            if (idx == 7):
                self.remainder1 = self.remainder0 % 100
                return self.remainder0 // 100
            if (idx == 8):
                self.remainder2 = self.remainder1 % 10
                return self.remainder1 // 10
            if (idx == 9):
                return self.remainder2 // 1

        # 10000자리일때
        if (score >= 10000 and score <= 99999):
            if (idx == 5):
                self.remainder0 = score % 10000
                return score // 10000
            if (idx == 6):
                self.remainder1 = self.remainder0 % 1000
                return self.remainder0 // 1000
            if (idx == 7):
                self.remainder2 = self.remainder1 % 100
                return self.remainder1 // 100
            if (idx == 8):
                self.remainder3 = self.remainder2 % 10
                return self.remainder2 // 10
            if (idx == 9):
                return self.remainder3 // 1


    def Update(self, player = None):
        #플레이어가 가지고 있는 웨폰레벨을 전달한다.
        self.Weapon.Update(player.WeaponLV)

        #플레이어가 가지고 있는 폭탄 UI update
        self.Bomb.Update()

        self.score = player.Score
        #점수가 갱신되지않으면 계산해서 ui가 업데이트 되지도 않는다.
        if (self.tmpscore != self.score):
            #index로 자리수를 구별한다
            for ui in self.ScoreList:
                idx = self.ScoreList.index(ui)
                #idx는 몇번째자리 점수인지 판별하는 인덱스이다.
                #왼쪽(제일 큰자리)부터 인덱스가 시작된다.
                score = self.CheckScore(idx , self.score)
                ui.Update(idx, score)
                #갱신된 점수를 저장한다.
                self.tmpscore = self.score






