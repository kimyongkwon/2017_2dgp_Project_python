from numpy import *
from pico2d import *
import time
import random
import pickle

#GameInfo
WIDTH= 700
HEIGHT= 900
SCORE = 0

#Player
PLAYER_SPEED = 300
UP_KEY, DOWN_KEY, RIGHT_KEY, LEFT_KEY, SHOT_KEY  = 0,1,2,3,4
A_KEY = 100

#BulletInfo
LEFT_DIRECTION, RIGHT_DIRECTION = 100, 200
BULLET_SPEED = 1000.0

#EnergyBulletInfo
MAX_ENERGY_GAUGE = 200

#BombShipInfo
BOMB_SHIP_SPEED = 500
BOMB_SHIP_WIDTH = 500
BOMB_SHIP_HEIGHT = 600
BOMB_EFFECT_POINT = 100

#MapScrollingInfo
SCROLLING_SPEED = 700

#ItemInfo
BOMB_ITEM_SPEED = 200
ITEM_MOVING_R_U, ITEM_MOVING_L_U, ITEM_MOVING_L_D, ITEM_MOVING_R_D= 0, 1, 2, 3

#EmenyShipInfo
OBJECT, DELAY = 0,1

#BombUIInfo
BOMB_UI_WIDTH = 40
BOMB_UI_HEIGHT = 40

ENEMY_SHIP_00_SPEED = 100
ENEMY_SHIP_00_WIDTH = 50
ENEMY_SHIP_00_HEIGHT = 50
ENEMY_SHIP_00_HP = 10
ENEMY_SHIP_00_BULLET_SPEED = 400

ENEMY_SHIP_01_SPEED = 150
ENEMY_SHIP_01_WIDTH = 50
ENEMY_SHIP_01_HEIGHT = 50
ENEMY_SHIP_01_HP = 10
ENEMY_SHIP_01_BULLET_SPEED = 200

ENEMY_SHIP_02_SPEED = 100
ENEMY_SHIP_02_WIDTH = 70
ENEMY_SHIP_02_HEIGHT = 70
ENEMY_SHIP_02_HP = 10
ENEMY_SHIP_02_BULLET_SPEED = 200
ENEMY_SHIP_02_MOVING_STOP, ENEMY_SHIP_02_MOVING_RIGHT, ENEMY_SHIP_02_MOVING_LEFT = 0, 1, 2

ENEMY_SHIP_03_SPEED = 100
ENEMY_SHIP_03_WIDTH = 50
ENEMY_SHIP_03_HEIGHT = 50
ENEMY_SHIP_03_HP = 10
ENEMY_SHIP_03_BULLET_SPEED = 100

BOSS_SHIP_00_SPEED = 100
BOSS_SHIP_00_WIDTH = 300
BOSS_SHIP_00_HEIGHT = 250
BOSS_SHIP_00_HP = 300
BOSS_SHIP_00_BULLET_SPEED = 100
BOSS_SHIP_00_MOVING_STOP, BOSS_SHIP_00_MOVING_RIGHT, BOSS_SHIP_00_MOVING_LEFT, BOSS_SHIP_00_MOVING_FORWARD, BOSS_SHIP_00_MOVING_BACK= 0, 1, 2, 3, 4