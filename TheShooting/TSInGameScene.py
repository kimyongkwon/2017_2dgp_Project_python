import TSRoomScene
import game_framework

from GameOverScene import *
from ShowRepertory import *

from Item.Item import *
from Ship.EnemyShip import *
from Ship.PlayerShip import *
from UI.GameUi import *
from Sound.Sound import *

name = "InGameScene"
player1 = None
background = None
Repertory = None
RepertoryManager = []
IngameSound = None
GameOver = None
Pause = None
tmpTime = 0
lock = False

#Repertory를 관리한다. 모든 레퍼토리들은 False였다가 True가 되면 활성화돼서 적들이 등장한다.
for n in range(0,10):
    RepertoryManager.append(False)

GameUI = None

#시간에 따라 객체들이 움직인다.
start_time = 0
frame_time = 0

class BackGround:
    def __init__(self):
        self.BackGroundImage = load_image('TSData/IG_background.png')
        self.screen_width, self.screen_height = 700, 900
        self.up = 0
        self.speed = 0
    def draw(self):
        y = int(self.up)
        h = min(self.screen_height, self.BackGroundImage.h - y)
        self.BackGroundImage.clip_draw_to_origin(0, y, self.screen_width, h, 0, 0)
        self.BackGroundImage.clip_draw_to_origin(0, 0, self.screen_width, self.screen_height-h, 0, h)
    def Update(self, frame_time):
        self.speed = SCROLLING_SPEED
        self.up = (self.up + frame_time * self.speed) % self.BackGroundImage.h

def enter():
    global player1
    global background
    global GameUI
    global Repertory
    global IngameSound
    global GameOver
    global Pause

    GameUI = UIList()
    player1 = MyShip()
    background = BackGround()
    Repertory = EnemyRepertory()
    IngameSound = InGameSound()
    GameOver = GameOverUI()
    Pause = False

    load_highscore()

def exit():
    global player1
    global background
    global GameUI
    global Repertory
    global IngameSound
    global GameOver
    global Pause

    del(player1)
    del(background)
    del(GameUI)
    del(Repertory)
    del(IngameSound)
    del(GameOver)
    del(Pause)

def handle_events():
    global x
    global player1
    global Pause
    global tmpTime
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_MOUSEMOTION:
            x, y = event.x, 600 - event.y
        elif event.type == SDL_KEYDOWN:
            if event.key == SDLK_ESCAPE:
                game_framework.change_state(TSRoomScene)
            elif event.key == SDLK_p:
                if (Pause): Pause = False
                else:
                    tmpTime = get_time()
                    Pause = True

        if (player1 != None): player1.Key_Event(event)

def PlayerAndBulletUpdate(frame_time):
    global player1

    # PlayerUpdate
    player1.update(frame_time)

    # BulletUpdate
    for weapon in player1.Weapons:
        if (weapon != None):
            for bullet in weapon:
                bullet.Update(frame_time)
                # 화면 밖을 벗어나면 제거한다.
                if (bullet.DeleteBullet(WIDTH, 0, HEIGHT, -20)):
                    weapon.remove(bullet)

    # EnergyBulletUpdate
    if (player1.EnergyBullet != None):
        player1.EnergyBullet.Update(frame_time)
        #deletetime이 일정시간이 지나면 에너지샷이 없어진다.
        if (player1.EnergyBullet.DeleteTime > 100):
            player1.EnergyBullet = None

    # BombBulletUpdate
    if (player1.BombShip != None):
        player1.BombShip.Update(frame_time)
        # BombEffect가 준비완료되면 BombBullet 업데이트
        if (player1.BombShip.ReadyBombEffect == True):
            if (player1.BombBullet != None):
                for BombBullet in player1.BombBullet:
                    BombBullet.Update()
                    #BombShip이 가지고 있는 이펙트가 다 끝났는지 확인한다. 그리고 끝났으면 Bomb이펙트 효과를 삭제한다.
                    if (BombBullet.DeleteBombBullet()):
                        player1.BombBullet.remove(BombBullet)

        # 만약 BombShip이 화면밖으로 나가서 delete되면 BombShip 초기화
        if (player1.BombShip.DeleteShip()):
            del player1.BombShip
            player1.BombShip = None



def ItemUpdate(frame_time):
    # ItemUpdate
    for Item in Items:
        if (Item != None):
            Item.Update(frame_time)

def CheckRepertory():
    global RepertoryManager

    time = get_time()

    # if문을 안들어가면 해당 레퍼토리매니져 인덱스는 None값을 가지게 된다.
    if ( time > 3.0 and  time < 15.0):  RepertoryManager[0] = True
    if ( time > 15.0 ): RepertoryManager[0] = None

    if ( time > 5.0 and  time < 30.0): RepertoryManager[1] = True
    if ( time > 30.0): RepertoryManager[1] = None

    if ( time > 7.0 and time < 30.0): RepertoryManager[2] = True
    if ( time > 30.0): RepertoryManager[2] = None

    if ( time > 9.0 and time < 30.0): RepertoryManager[3] = True
    if ( time > 30.0): RepertoryManager[3] = None

    if ( time > 12.0 and time < 30.0): RepertoryManager[4] = True
    if ( time > 30.0): RepertoryManager[4] = None

def update():
    global start_time
    global frame_time
    global Repertory
    global player1
    global Pause
    global tmpTime
    global lock

    if (Pause == False):
        frame_time = get_time() - start_time
        background.Update(frame_time)

        #흘러간 시간에 따라 적기들의 등장을 관리한다.
        CheckRepertory()

        k = 0
        for i in range(0, 10):

            # 만약 해당 레퍼토리매니저인덱스가 True가 되면 레퍼토리 인덱스와 frame_time을 Repertory Update함수로 넘겨준다.
            if (RepertoryManager[i] == True):
                Repertory.Update(i, frame_time, player1)
                if (player1 != None): Repertory.CheckCollision(player1)

            # 만약 해당 레퍼토리매니저인덱스가 None이 되면 더이상 게임에 등장시키지 않아도 된다.
            # 그러므로 그 레퍼토리를 지운다. ((1))
            if (RepertoryManager[i] == None):
                k += 1
                Repertory.Delete(i)

        #case1 : 게임 클리어했을때
        if (k == 5 and player1 != None):
            GameOver.Object = ScoreEntry(player1.Score, time.time())
            GameOver.Add_Score()
            save_highscores()
            player1 = None
            GameOver.Ending('Congratulation')

        #Player에 관한것들을 Update한다.
        #UI도 모두 Player와 관련된것들이다.
        if (player1 != None):
            PlayerAndBulletUpdate(frame_time)
            GameUI.Update(player1)
            #case2 : 게임 오버됐을때
            #만약 Player가 피격되면 게임오버 된다.
            if (player1.GameOver == True):
                #GameOver.Object = Object(score=player1.Score, time=time.time())
                GameOver.Object = ScoreEntry(player1.Score, time.time())
                GameOver.Add_Score()
                save_highscores()
                player1 = None
                GameOver.Ending('GameOver')

        if (GameOver.State != None): GameOver.Update()

        if (player1 != None): Items.Update(frame_time, player1)

        start_time += frame_time

    #delay(0.01)

def draw():
    clear_canvas()
    background.draw()

    if (player1 != None):
        player1.draw()

        #BulletDraw
        for weapon in player1.Weapons:
            if (weapon != None):
                for bullet in weapon:
                    bullet.Draw()

        #EnergyBulletDraw
        if (player1.EnergyBullet != None):
            player1.EnergyBullet.Draw()

        #BombShip
        if (player1.BombShip != None):
            player1.BombShip.Draw()
            #만약 BombShip이 폭발효과가 준비되어있다면 Draw
            if (player1.BombShip.ReadyBombEffect == True):
                for BombBullet in player1.BombBullet:
                    if (BombBullet != None):
                        BombBullet.Draw()

    #UIDraw
    GameUI.Draw()

    Items.Draw()

    #게임오버되면 GameOverUI를 Draw한다
    if (player1 == None):
        GameOver.Draw()

    for i in range(0, 10):
        if (RepertoryManager[i] == True):
            Repertory.Draw(i)

    update_canvas()