from stdafx import *
from Sound.Sound import *

#########################################################################################
##
class Item:
    def __init__(self): pass

    def GetBoundingBox(self):
        return self.x , self.y, self.x + self.w, self.y + self.h

    def DrawBoundingBox(self):
        draw_rectangle(*self.GetBoundingBox())

#########################################################################################
##
class GameItem(Item):
    def __init__(self, ItemType, initX, initY):
        self.w, self.h = 30, 30
        self.PosVector = initX, initY
        self.MovingVector = array([[1, 1]], dtype=float32)
        self.MoveDistance = 0
        self.Image = None
        self.ItemType = ItemType
        if (self.Image == None):
            if (self.ItemType == 'PowerUp'):
                self.Image = load_image('TSData/PowerUpItem.png')
            if (self.ItemType == 'Bomb'):
                self.Image = load_image('TSData/BombItem.png')

        self.x, self.y = 0,0

        self.NowMoveState = random.randint(0,3)

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.MovingVector
        del self.MoveDistance
        del self.Image
        del self.PosVector

    def Update(self, frame_time):
        self.MoveUpdate(frame_time)

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

    def MoveUpdate(self, frame_time):
        self.MoveDistance = BOMB_ITEM_SPEED * frame_time
        if (self.NowMoveState == ITEM_MOVING_R_U):
            self.Moving_State[ITEM_MOVING_R_U] (self)
        if (self.NowMoveState == ITEM_MOVING_L_U):
            self.Moving_State[ITEM_MOVING_L_U](self)
        if (self.NowMoveState == ITEM_MOVING_L_D):
            self.Moving_State[ITEM_MOVING_L_D](self)
        if (self.NowMoveState == ITEM_MOVING_R_D):
            self.Moving_State[ITEM_MOVING_R_D](self)

    def MovingVecter(self, Direction):
        if (Direction == ITEM_MOVING_R_U):
            self.MovingVector = array([[1, 1]], dtype=float32)
        if (Direction == ITEM_MOVING_L_U):
            self.MovingVector = array([[-1, 1]], dtype=float32)
        if (Direction == ITEM_MOVING_L_D):
            self.MovingVector = array([[-1, -1]], dtype=float32)
        if (Direction == ITEM_MOVING_R_D):
            self.MovingVector = array([[1, -1]], dtype=float32)
        return self.MovingVector

    def MovingState_R_U(self):
        self.PosVector += self.MovingVecter(ITEM_MOVING_R_U) * self.MoveDistance
        if (self.x + self.w > WIDTH):
            self.NowMoveState = ITEM_MOVING_L_U
        if (self.y + self.h > HEIGHT):
            self.NowMoveState = ITEM_MOVING_R_D

    def MovingState_L_U(self):
        self.PosVector += self.MovingVecter(ITEM_MOVING_L_U) * self.MoveDistance
        if (self.y + self.h > HEIGHT):
            self.NowMoveState = ITEM_MOVING_L_D
        if (self.x < 0):
            self.NowMoveState = ITEM_MOVING_R_U

    def MovingState_L_D(self):
        self.PosVector += self.MovingVecter(ITEM_MOVING_L_D) * self.MoveDistance
        if (self.x < 0):
            self.NowMoveState = ITEM_MOVING_R_D
        if (self.y < 0):
            self.NowMoveState = ITEM_MOVING_L_U

    def MovingState_R_D(self):
        self.PosVector += self.MovingVecter(ITEM_MOVING_R_D) * self.MoveDistance
        if (self.y < 0):
            self.NowMoveState = ITEM_MOVING_R_U
        if (self.x + self.w > WIDTH):
            self.NowMoveState = ITEM_MOVING_L_D

    Moving_State = {
        ITEM_MOVING_R_U: MovingState_R_U,
        ITEM_MOVING_L_U: MovingState_L_U,
        ITEM_MOVING_L_D: MovingState_L_D,
        ITEM_MOVING_R_D: MovingState_R_D
    }

    def Draw(self):
        self.Image.clip_draw_to_origin(0, 0, 120, 110, self.x, self.y, self.w, self.h)

class ItemList:
    def __init__(self):
        self.Items = []

    def __del__(self):
        del self.Items

    def addItem(self, object):
        self.Items.append(object)

    def Update(self, frame_time, player):
        for item in self.Items:
            item.Update(frame_time)
            if (player.CheckCollision(item)):
                if (item.ItemType == 'PowerUp'): Sounds('item'), player.WeaponUpgrade()
                elif (item.ItemType == 'Bomb'): Sounds('item')
                self.Items.remove(item)

    def Draw(self):
        for item in self.Items:
            item.Draw()

Items = ItemList()

