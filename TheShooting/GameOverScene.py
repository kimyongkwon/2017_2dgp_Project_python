
from stdafx import *

high_scores = []
filename = "highscores.pickle"

###########################################################################################################
## 랭킹 관리
class ScoreEntry:
    def __init__(self, score, time):
        self.score = score
        self.time = time

def load_highscore():
    global high_scores, filename
    high_scores = []
    # load scores from disk
    try:
        f = open(filename, "rb")
        high_scores = pickle.load(f)
        f.close()
    except FileNotFoundError:
        print("No highscore file")

def save_highscores():
    global high_scores, filename
    # save scores to disk
    f = open(filename, "wb")
    pickle.dump(high_scores, f)
    f.close()

def delete_highscores():
    global high_scores
    high_scores = []
    save_highscores()

###########################################################################################################
## GameOver
class GameOverUI:
    def __init__(self):
        self.x, self.y = 50, -300
        self.w, self.h = 600,250
        self.GameOverimage = load_image('TSData/GameOver.png')
        self.Victoryimage = load_image('TSData/congratulation.png')
        self.font = load_font('TSData/ConsolaMalgun.ttf', 30)
        self.Object = None
        self.State = None
        self.score_y = -350

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.GameOverimage
        del self.Victoryimage
        del self.Object
        del self.font
        del self.score_y
        del self.State

    def Ending(self, state):
        self.State = state

    def Update(self):
        if (self.y < 500):
            self.y += 3
        if (self.score_y < 450):
            self.score_y += 3

    def Draw(self):
        if (self.State == 'GameOver'):
            self.GameOverimage.clip_draw_to_origin(0,0,800,200, self.x, self.y, self.w, self.h)
        elif (self.State == 'Congratulation'):
            self.Victoryimage.clip_draw_to_origin(0,0,1400, 180, self.x, self.y, self.w, self.h)

        no = 1
        for e in high_scores:
            str = "{:2d} {:5.1f}".format(no, e.score)
            self.font.draw(40, self.score_y - (50 * no), str, (255, 255, 255))
            self.font.draw(230, self.score_y - (50 * no), time.asctime(time.localtime(e.time)), (223, 255, 223))
            no += 1

    def Add_Score(self):
        inserted = False
        for i in range(len(high_scores)):
            e = high_scores[i]
            if (e.score < self.Object.score):
                high_scores.insert(i, self.Object)
                inserted = True
                break
        if (not inserted):
            high_scores.append(self.Object)

        if (len(high_scores) > 5):
            high_scores.pop(-1)

