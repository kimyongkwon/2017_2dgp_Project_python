from Ship.EnemyShip import *
from Item.Item import *
from stdafx import *

name = 'ShowRepertory'

class EnemyShow:
    #EnemyShowXX들은 모두 CheckCollision이라는 함수를 공통으로 사용한다.
    def CheckCollision(self, player):
        #플레이어bullet과 적기의 0충돌
        for weapon in player.Weapons:
            if (weapon != None ):
                for bullet in weapon:
                    for EnemyShip in self.EnemyShips:
                        if (EnemyShip[OBJECT] != None):
                            #충돌하면 EnemyShip HP를 한개씩 깍는다.
                            if (EnemyShip[OBJECT].CheckCollision(bullet)):
                                EnemyShip[OBJECT].HP -= 1
                                player.AddScore(EnemyShip[OBJECT], 1)
                                weapon.remove(bullet)
                                Sounds('damaged')
                            #EnemyShip HP가 0이 되면 제거
                            if (EnemyShip[OBJECT].HP == 0):
                                player.AddScore(EnemyShip[OBJECT], 2)
                                rand = random.randint(0, 100)
                                # 랜덤하게 item이 생성된다.
                                if rand >= 0 and rand <= 50: Items.addItem(GameItem('PowerUp', EnemyShip[OBJECT].x, EnemyShip[OBJECT].y))
                                #if rand >= 20 and rand <= 40: Items.addItem(GameItem('Bomb', EnemyShip[OBJECT].x, EnemyShip[OBJECT].y))
                                self.EnemyShips.remove(EnemyShip)
                                Sounds('down')
                                #이 밑으로 아이템이 나오도록 해야함

        #플레이어와 적기의 충돌
        for EnemyShip in self.EnemyShips:
            if (EnemyShip[OBJECT] != None):
                if (EnemyShip[OBJECT].CheckCollision(player)):
                    # 충돌하면 플레이어의 무기레벨을 다운그레이드한다. 1이하로 내려가지 않는다.
                    player.WeaponDowngrade()

        #플레이어EnergyBullet과 적기의 충돌
        if(player.EnergyBullet != None):
            for EnemyShip in self.EnemyShips:
                if (EnemyShip[OBJECT].CheckCollision(player.EnergyBullet)):
                    self.EnemyShips.remove(EnemyShip), print("적기 다운")

        if (player.BombShip != None and player.BombShip.ReadyBombEffect == True):
            for EnemyShip in self.EnemyShips:
                #맵안에 있는 적들만 섬멸한다.
                if (EnemyShip[OBJECT].x + (EnemyShip[OBJECT].w/2) > 0 and
                    EnemyShip[OBJECT].x + (EnemyShip[OBJECT].w / 2) < WIDTH and
                    EnemyShip[OBJECT].y + (EnemyShip[OBJECT].h / 2) > 0 and
                    EnemyShip[OBJECT].y + (EnemyShip[OBJECT].h / 2) < WIDTH ):
                    Sounds('bomb')
                    player.AddScore(EnemyShip[OBJECT], 2)
                    self.EnemyShips.remove(EnemyShip)


        #플레이어와 적기Bullet의 충돌체크
        for bullet in self.EnemyBullet:
            if (bullet != None):
                if(bullet.CheckCollision(player)):
                    player.GameOver = True
                    Sounds('damaged')


    def Draw(self):
        #EnemyShip Draw
        for EnemyShip in self.EnemyShips:
            EnemyShip[OBJECT].Draw()

        #EnemyShipBullet Draw
        for EnemyBullet in self.EnemyBullet:
            EnemyBullet.Draw()

    def Delete(self):
        #EnemyShips을 지운다. ((3))
        for EnemyShip in self.EnemyShips:
            self.EnemyShips.remove(EnemyShip)
            #만약 밑에 조건이 충족하면 EnemyShowXX 클래스객체를 삭제 할 수 있다.
            #EnemyShowXX의 클래스객체는 EnemyShowXX들을 관리하는 EnemyRepertory에서 삭제한다.
            if (len(self.EnemyShips) == 0):
                print("EnemyShips 완전 제거")
                return True

        if (len(self.EnemyShips) == 0):
            print("EnemyShips 완전 제거")
            return True


class EnemyShow00(EnemyShow):
    def __init__(self):
        #EnemyShow는 EnemyShip들과 그것들의 탄환을 관리한다.
        self.EnemyShips = []
        #두번째 인자는 탄환의 delay를 나타낸다.
        self.EnemyShips.append([EnemyShip00(100,1000), 0])
        self.EnemyShips.append([EnemyShip00(200,1000), 0])
        self.EnemyShips.append([EnemyShip00(300,1000), 0])
        self.EnemyShips.append([EnemyShip00(400,1000), 0])
        self.EnemyShips.append([EnemyShip00(500,1000), 0])

        #EnemyBullet은 EnemyShip이 가지고 있지않고, EnemyShow가 관리한다.
        self.EnemyBullet = []

    def __del__(self):
        #((5))
        print("EnemyShow00제거")
        del self.EnemyShips
        del self.EnemyBullet

    def CreateBullet(self, Delay, EnemyShip):
        EnemyShip[DELAY] += 1
        if (EnemyShip[DELAY] >= Delay):
            self.EnemyBullet.append(Enemy00Bullet(*EnemyShip[OBJECT].GetBulletPos()))
            EnemyShip[DELAY]  = 0

    def Update(self, frame_time):
        for EnemyShip in self.EnemyShips:
            self.CreateBullet(200, EnemyShip)
            EnemyShip[OBJECT].Update(frame_time)

        for EnemyBullet in self.EnemyBullet:
            EnemyBullet.Update(frame_time)
            if(EnemyBullet.DeleteBullet(WIDTH,0,HEIGHT,-20)):
                #print('enemyBullet 제거')
                self.EnemyBullet.remove(EnemyBullet)

class EnemyShow01(EnemyShow):
    def __init__(self):
        #EnemyShow는 EnemyShip들과 그것들의 탄환을 관리한다.
        self.EnemyShips = []
        #두번째 인자는 탄환의 delay를 나타낸다.
        self.EnemyShips.append([EnemyShip01(100,1000), 0])
        self.EnemyShips.append([EnemyShip01(200,1000), 0])
        self.EnemyShips.append([EnemyShip01(300,1000), 0])
        self.EnemyShips.append([EnemyShip01(400,1000), 0])
        self.EnemyShips.append([EnemyShip01(500,1000), 0])

        #EnemyBullet은 EnemyShip이 가지고 있지않고, EnemyShow가 관리한다.
        self.EnemyBullet = []

    def __del__(self):
        #((5))
        print("EnemyShow01제거")
        del self.EnemyShips
        del self.EnemyBullet

    def ComputeSpreadBullet(self, Delay, EnemyShip):
        vx0 , vy0 = 0, -1
        Num , theta = 4, 20
        vx , vy = 0, 0
        rad = 0
        rad_step = pi / 180 * theta

        # 탄환 각도 계산
        if (Num % 2 == 1):
            rad = (-Num / 2) * rad_step
        elif (Num % 2 == 0):
            rad = (-Num / 2 + 0.5) * rad_step

        EnemyShip[DELAY] += 1
        if (EnemyShip[DELAY] >= Delay):
            for k in range(0, Num):
                c = cos(rad)
                s = sin(rad)
                vx = vx0 * c - vy0 * s
                vy = vx0 * s + vy0 * c
                self.EnemyBullet.append(Enemy01Bullet((EnemyShip[OBJECT].x + EnemyShip[OBJECT].w / 2), (EnemyShip[OBJECT].y + EnemyShip[OBJECT].h / 2), vx, vy))
                rad += rad_step
            EnemyShip[DELAY] = 0

    def Update(self, frame_time):
        for EnemyShip in self.EnemyShips:
            self.ComputeSpreadBullet(200, EnemyShip)
            EnemyShip[OBJECT].Update(frame_time)

        for EnemyBullet in self.EnemyBullet:
            EnemyBullet.Update(frame_time)
            if(EnemyBullet.DeleteBullet(WIDTH,0,HEIGHT,-20)):
                self.EnemyBullet.remove(EnemyBullet)

class EnemyShow02(EnemyShow):
    def __init__(self):
        # EnemyShow는 EnemyShip들과 그것들의 탄환을 관리한다.
        self.EnemyShips = []
        # 두번째 인자는 탄환의 delay를 나타낸다.
        self.EnemyShips.append([EnemyShip02(WIDTH/2 - 30, 1200), 0])

        # EnemyBullet은 EnemyShip이 가지고 있지않고, EnemyShow가 관리한다.
        self.EnemyBullet = []

    def __del__(self):
        # ((5))
        print("EnemyShow02제거")
        del self.EnemyShips
        del self.EnemyBullet

    def ComputeSpreadBullet(self, Delay, EnemyShip):
        vx0, vy0 = 0, -1
        Num, theta = 36, 10
        vx, vy = 0, 0
        rad = 0
        rad_step = 3.14 / 180 * theta

        # 탄환 각도 계산
        if (Num % 2 == 1):
            rad = (-Num / 2) * rad_step
        elif (Num % 2 == 0):
            rad = (-Num / 2 + 0.5) * rad_step

        EnemyShip[DELAY] += 1
        if (EnemyShip[DELAY] >= Delay):
            for k in range(0, Num):
                rad += rad_step
                c = cos(rad)
                s = sin(rad)
                vx = vx0 * c - vy0 * s
                vy = vx0 * s + vy0 * c
                self.EnemyBullet.append(Enemy01Bullet((EnemyShip[OBJECT].x + EnemyShip[OBJECT].w / 2), (EnemyShip[OBJECT].y + EnemyShip[OBJECT].h / 2), vx,vy))
                rad += rad_step
            EnemyShip[DELAY] = 0

    def Update(self, frame_time):
        for EnemyShip in self.EnemyShips:
            self.ComputeSpreadBullet(300, EnemyShip)
            EnemyShip[OBJECT].Update(frame_time)

        for EnemyBullet in self.EnemyBullet:
            EnemyBullet.Update(frame_time)
            if (EnemyBullet.DeleteBullet(WIDTH, 0, HEIGHT, -20)):
                self.EnemyBullet.remove(EnemyBullet)

class EnemyShow03(EnemyShow):
    def __init__(self):
        # EnemyShow는 EnemyShip들과 그것들의 탄환을 관리한다.
        self.EnemyShips = []
        # 두번째 인자는 탄환의 delay를 나타낸다.
        self.EnemyShips.append([EnemyShip03(300,1000), 0])

        # EnemyBullet은 EnemyShip이 가지고 있지않고, EnemyShow가 관리한다.
        self.EnemyBullet = []

    def __del__(self):
        # ((5))
        print("EnemyShow03제거")
        del self.EnemyShips
        del self.EnemyBullet

    def MoveHomingBullet(self, Delay, EnemyShip, player):
            EnemyShip[DELAY] += 1
            if (EnemyShip[DELAY] >= Delay):
                d = sqrt( (player.x - EnemyShip[OBJECT].x) * (player.x - EnemyShip[OBJECT].x) + (player.y - EnemyShip[OBJECT].y) * (player.y - EnemyShip[OBJECT].y) )
                vx = (player.x - EnemyShip[OBJECT].x) / d * 3
                vy = (player.y - EnemyShip[OBJECT].y) / d * 3
                self.EnemyBullet.append( Enemy02Bullet((EnemyShip[OBJECT].x + EnemyShip[OBJECT].w / 2), (EnemyShip[OBJECT].y + EnemyShip[OBJECT].h / 2), vx , vy) )
                EnemyShip[DELAY] = 0

    def Update(self, frame_time, player):
        for EnemyShip in self.EnemyShips:
            if (player != None): self.MoveHomingBullet(50, EnemyShip, player)
            EnemyShip[OBJECT].Update(frame_time)

        for EnemyBullet in self.EnemyBullet:
            EnemyBullet.Update(frame_time)
            if (EnemyBullet.DeleteBullet(WIDTH, 0, HEIGHT, -20)):
                self.EnemyBullet.remove(EnemyBullet)

class EnemyShow04(EnemyShow):
    def __init__(self):
        # EnemyShow는 EnemyShip들과 그것들의 탄환을 관리한다.
        self.EnemyShips = []
        # 두번째 인자는 탄환의 delay를 나타낸다.
        self.EnemyShips.append([BossShip00(WIDTH/2 - 150,1000), 0])

        # EnemyBullet은 EnemyShip이 가지고 있지않고, EnemyShow가 관리한다.
        self.EnemyBullet = []

    def __del__(self):
        # ((5))
        print("EnemyShow04제거")
        del self.EnemyShips
        del self.EnemyBullet

    #보스는 방향탄과 원형탄을 같이 쏜다.
    def MoveHomingANDSpreadBullet(self, Delay, EnemyShip, player):
            vx0, vy0 = 0, -1
            Num, theta = 50, 5
            vx, vy = 0, 0
            rad = 0
            rad_step = 3.14 / 180 * theta

            # 탄환 각도 계산
            if (Num % 2 == 1):
                rad = (-Num / 2) * rad_step
            elif (Num % 2 == 0):
                rad = (-Num / 2 + 0.5) * rad_step

            EnemyShip[DELAY] += 1
            if (EnemyShip[DELAY] >= Delay):
                for k in range(0, Num):
                    rad += rad_step
                    c = cos(rad)
                    s = sin(rad)
                    vx = vx0 * c - vy0 * s
                    vy = vx0 * s + vy0 * c
                    self.EnemyBullet.append(Enemy01Bullet((EnemyShip[OBJECT].x + EnemyShip[OBJECT].w / 2), (EnemyShip[OBJECT].y + EnemyShip[OBJECT].h / 2), vx, vy))
                    rad += rad_step

                d = sqrt( (player.x - EnemyShip[OBJECT].x) * (player.x - EnemyShip[OBJECT].x) + (player.y - EnemyShip[OBJECT].y) * (player.y - EnemyShip[OBJECT].y) )
                vx = (player.x - EnemyShip[OBJECT].x) / d * 3
                vy = (player.y - EnemyShip[OBJECT].y) / d * 3
                self.EnemyBullet.append( Enemy02Bullet((EnemyShip[OBJECT].x + EnemyShip[OBJECT].w / 2), (EnemyShip[OBJECT].y + EnemyShip[OBJECT].h / 2), vx , vy) )
                EnemyShip[DELAY] = 0

    def Update(self, frame_time, player):
        for EnemyShip in self.EnemyShips:
            if (player != None): self.MoveHomingANDSpreadBullet(100, EnemyShip, player)
            EnemyShip[OBJECT].Update(frame_time)

        for EnemyBullet in self.EnemyBullet:
            EnemyBullet.Update(frame_time)
            if (EnemyBullet.DeleteBullet(WIDTH, 0, HEIGHT, -20)):
                self.EnemyBullet.remove(EnemyBullet)

#EnemyRepertory는 EnemyShowXX, EnemyShowXX는 해당 레퍼토리나오는 EnemyShips들을 관리한다.
class EnemyRepertory:
    def __init__(self):
        self.Repertorys = []
        self.Repertorys.append(EnemyShow00())
        self.Repertorys.append(EnemyShow01())
        self.Repertorys.append(EnemyShow02())
        self.Repertorys.append(EnemyShow03())
        self.Repertorys.append(EnemyShow04())

    def __del__(self):
        # ((6))
        for Repertory in self.Repertorys:
            if (Repertory == None):
                del Repertory

        '''    
        if (self.Repertory00 == None): del self.Repertory00
        if (self.Repertory01 == None): del self.Repertory01
        if (self.Repertory02 == None): del self.Repertory02
        if (self.Repertory03 == None): del self.Repertory03
        if (self.Repertory04 == None): del self.Repertory04
        '''

    #RepertoryXX의 업데이트를 관리한다.
    def Update(self, RepertoryIndex, frame_time, player):
        if (RepertoryIndex == 0): self.Repertorys[0].Update(frame_time)
        if (RepertoryIndex == 1): self.Repertorys[1].Update(frame_time)
        if (RepertoryIndex == 2): self.Repertorys[2].Update(frame_time)
        if (RepertoryIndex == 3): self.Repertorys[3].Update(frame_time, player)
        if (RepertoryIndex == 4): self.Repertorys[4].Update(frame_time, player)

        '''
        if (RepertoryIndex == 0): self.Repertory00.Update(frame_time)
        if (RepertoryIndex == 1): self.Repertory01.Update(frame_time)
        if (RepertoryIndex == 2): self.Repertory02.Update(frame_time)
        if (RepertoryIndex == 3): self.Repertory03.Update(frame_time, player)
        if (RepertoryIndex == 4): self.Repertory04.Update(frame_time, player)
        '''

    #수정필요
    def Draw(self, RepertoryIndex):
        for Repertory in self.Repertorys:
            if (Repertory != None and (self.Repertorys.index(Repertory) == RepertoryIndex)):
                Repertory.Draw()

        '''
        if (self.Repertory00 != None and RepertoryIndex == 0): self.Repertory00.Draw()
        if (self.Repertory01 != None and RepertoryIndex == 1): self.Repertory01.Draw()
        if (self.Repertory02 != None and RepertoryIndex == 2): self.Repertory02.Draw()
        if (self.Repertory03 != None and RepertoryIndex == 3): self.Repertory03.Draw()
        if (self.Repertory04 != None and RepertoryIndex == 4): self.Repertory04.Draw()
        '''


    def Delete(self, RepertoryIndex):
        # 래퍼토리에 있는 Ship객체들이 다 지워지면 래퍼토리도 지워준다(None) ((2)) , ((4))
        #밑에꺼랑 같은 로직인걱 같은데 왜 결과는 다르게 나오지?
        if (RepertoryIndex == 0):
            if (self.Repertorys[0] != None and self.Repertorys[0].Delete()): self.Repertorys[0] = None
        if (RepertoryIndex == 1):
            if (self.Repertorys[1]  != None and self.Repertorys[1].Delete()): self.Repertorys[1] = None
        if (RepertoryIndex == 2):
            if (self.Repertorys[2]  != None and self.Repertorys[2].Delete()): self.Repertorys[2] = None
        if (RepertoryIndex == 3):
            if (self.Repertorys[3]  != None and self.Repertorys[3].Delete()): self.Repertorys[3]= None
        if (RepertoryIndex == 4):
            if (self.Repertorys[4]  != None and self.Repertorys[4].Delete()): self.Repertorys[4] = None

        '''
        for Repertory in self.Repertorys:
            if (self.Repertorys.index(Repertory) == RepertoryIndex):
                if (Repertory != None and Repertory.Delete()):
                    Repertory = None
        '''


        '''
        if (RepertoryIndex == 0):
            if (self.Repertory00 != None and self.Repertory00.Delete()): self.Repertory00 = None
        if (RepertoryIndex == 1):
            if (self.Repertory01 != None and self.Repertory01.Delete()): self.Repertory01 = None
        if (RepertoryIndex == 2):
            if (self.Repertory02 != None and self.Repertory02.Delete()): self.Repertory02 = None
        if (RepertoryIndex == 3):
            if (self.Repertory03 != None and self.Repertory03.Delete()): self.Repertory03 = None
        if (RepertoryIndex == 4):
            if (self.Repertory04 != None and self.Repertory04.Delete()): self.Repertory04 = None
        '''

    def CheckCollision(self, player):
        for Repertory in self.Repertorys:
            if (Repertory != None):
                Repertory.CheckCollision(player)

        '''
        if (self.Repertory00 != None ): self.Repertory00.CheckCollision(player)
        if (self.Repertory01 != None): self.Repertory01.CheckCollision(player)
        if (self.Repertory02 != None): self.Repertory02.CheckCollision(player)
        if (self.Repertory03 != None): self.Repertory03.CheckCollision(player)
        if (self.Repertory04 != None): self.Repertory04.CheckCollision(player)
        '''











