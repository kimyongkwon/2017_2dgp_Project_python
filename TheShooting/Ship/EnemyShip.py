from Ship.Ship import *
from Bullet.EnemyBullet import *

#########################################################################################
##
class EnemyShip00(Ship):
    def __init__(self, initX = 0, initY = 0):
        #initX = random.randint(0, WIDTH - ENEMY_SHIP_01_WIDTH)
        self.PosVector = array([[initX, initY]], dtype=float32)
        self.MovingVector = array([[0, -1]], dtype=float32)
        self.x, self.y = self.PosVector[0][0], self.PosVector[0][1]
        self.w, self.h = ENEMY_SHIP_00_WIDTH, ENEMY_SHIP_00_HEIGHT
        self.Image = load_image('TSData/enemy1.png')
        self.HP = ENEMY_SHIP_00_HP
        self.tmpHP = self.HP
        self.type = 'EnemyShip00'

        #Collision Animation
        self.CollsionImage = load_image('TSData/enemy1_collision.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.Image
        del self.HP
        del self.tmpHP
        del self.PosVector
        del self.CollsionImage
        del self.type

    def Update(self, frame_time):
        MoveDistance = ENEMY_SHIP_00_SPEED * frame_time
        self.PosVector += self.MovingVector * MoveDistance

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

    def Draw(self):
        #hp가 깍이는것은 충돌했다는걸 의미하므로 충돌된 이미지를 렌더링한다.
        if (self.tmpHP != self.HP):
            self.CollsionImage.clip_draw_to_origin(0, 0, 126, 118, self.x, self.y, self.w, self.h)
        elif(self.tmpHP == self.HP):
            self.Image.clip_draw_to_origin(0, 0, 126, 118, self.x, self.y, self.w, self.h)
        self.tmpHP = self.HP


#########################################################################################
##
class EnemyShip01(Ship):
    MoveDistance = 0

    def __init__(self, initX = 0, initY = 0):
        self.PosVector = array([[initX, initY]], dtype=float32)
        self.MovingVector = array([[0, -1]], dtype=float32)
        self.x, self.y = self.PosVector[0][0], self.PosVector[0][1]
        self.w, self.h = ENEMY_SHIP_01_WIDTH, ENEMY_SHIP_01_HEIGHT
        self.Image = load_image('TSData/enemy3.png')
        self.HP = ENEMY_SHIP_01_HP
        self.tmpHP = self.HP
        self.type = 'EnemyShip01'
        # Collision Animation
        self.CollsionImage = load_image('TSData/enemy3_collision.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.Image
        del self.HP
        del self.tmpHP
        del self.PosVector
        del self.type
        del self.CollsionImage

    def Update(self, frame_time):
        MoveDistance = ENEMY_SHIP_01_SPEED * frame_time
        self.PosVector += self.MovingVector * MoveDistance

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

    def Draw(self):
        if (self.tmpHP != self.HP):
            self.CollsionImage.clip_draw_to_origin(0, 0, 142, 98, self.x, self.y, self.w, self.h)
        elif(self.tmpHP == self.HP):
            self.Image.clip_draw_to_origin(0, 0, 142, 98, self.x, self.y, self.w, self.h)
        self.tmpHP = self.HP


#########################################################################################
##
class EnemyShip02(Ship):
    MoveDistance = 0

    def __init__(self, initX=0, initY=0):
        self.PosVector = array([[initX, initY]], dtype=float32)
        self.MovingVector = array([[0, -1]], dtype=float32)
        self.x, self.y = self.PosVector[0][0], self.PosVector[0][1]
        self.w, self.h = ENEMY_SHIP_02_WIDTH, ENEMY_SHIP_02_HEIGHT
        self.Image = load_image('TSData/enemy2.png')
        self.HP = ENEMY_SHIP_02_HP
        self.tmpHP = self.HP
        self.type = 'EnemyShip02'
        # Collision Animation
        self.CollsionImage = load_image('TSData/enemy2_collision.png')
        self.lock = False

    def __del__(self):
        del self.PosVector
        del self.MovingVector
        del self.x, self.y
        del self.w, self.h
        del self.Image
        del self.HP
        del self.tmpHP
        del self.type
        del self.CollsionImage
        del self.lock


    def MovingState_Stop(self):
        self.MovingVector = array([[0, 0]], dtype=float32)
    def MovingState_Right(self):
        self.MovingVector = array([[1, 0]], dtype=float32)
    def MovingState_Left(self):
        self.MovingVector = array([[-1, 0]], dtype=float32)

    def Update(self, frame_time):
        MoveDistance = ENEMY_SHIP_02_SPEED * frame_time
        self.PosVector += self.MovingVector * MoveDistance

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

        #해당 EnemyShip은 특별한 움직임이 있다.
        if (self.y < 700 and self.lock == False):
            self.Moving_State[ENEMY_SHIP_02_MOVING_RIGHT](self)
            self.lock = True

        if (self.x > 550):
            self.Moving_State[ENEMY_SHIP_02_MOVING_LEFT](self)

        if (self.x < 100):
            self.Moving_State[ENEMY_SHIP_02_MOVING_RIGHT](self)


    Moving_State = {
        ENEMY_SHIP_02_MOVING_STOP: MovingState_Stop,
        ENEMY_SHIP_02_MOVING_RIGHT: MovingState_Right,
        ENEMY_SHIP_02_MOVING_LEFT: MovingState_Left
    }

    def Draw(self):
        if (self.tmpHP != self.HP):
            self.CollsionImage.clip_draw_to_origin(0, 0, 180, 90, self.x, self.y, self.w, self.h)
        elif(self.tmpHP == self.HP):
            self.Image.clip_draw_to_origin(0, 0, 180, 90, self.x, self.y, self.w, self.h)
        self.tmpHP = self.HP

#########################################################################################
##
class EnemyShip03(Ship):
    def __init__(self, initX = 0, initY = 0):
        #initX = random.randint(0, WIDTH - ENEMY_SHIP_01_WIDTH)
        self.PosVector = array([[initX, initY]], dtype=float32)
        self.MovingVector = array([[0, -1]], dtype=float32)
        self.x, self.y = self.PosVector[0][0], self.PosVector[0][1]
        self.w, self.h = ENEMY_SHIP_03_WIDTH, ENEMY_SHIP_03_HEIGHT
        self.Image = load_image('TSData/enemy4.png')
        self.HP = ENEMY_SHIP_03_HP
        self.tmpHP = self.HP
        self.type = 'EnemyShip03'
        # Collision Animation
        self.CollsionImage = load_image('TSData/enemy4_collision.png')
        self.lock = False

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.Image
        del self.HP
        del self.tmpHP
        del self.PosVector
        del self.type
        del self.CollsionImage
        del self.lock

    def Update(self, frame_time):
        MoveDistance = ENEMY_SHIP_03_SPEED * frame_time
        self.PosVector += self.MovingVector * MoveDistance

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

    def Draw(self):
        if (self.tmpHP != self.HP):
            self.CollsionImage.clip_draw_to_origin(0, 0, 179, 154, self.x, self.y, self.w, self.h)
        elif(self.tmpHP == self.HP):
            self.Image.clip_draw_to_origin(0, 0, 179, 154, self.x, self.y, self.w, self.h)
        self.tmpHP = self.HP

#########################################################################################
##
class BossShip00(Ship):
    def __init__(self, initX = 0, initY = 0):
        #initX = random.randint(0, WIDTH - ENEMY_SHIP_01_WIDTH)
        self.PosVector = array([[initX, initY]], dtype=float32)
        self.MovingVector = array([[0, -1]], dtype=float32)
        self.x, self.y = self.PosVector[0][0], self.PosVector[0][1]
        self.w, self.h = BOSS_SHIP_00_WIDTH, BOSS_SHIP_00_HEIGHT
        self.Image = load_image('TSData/boss3.png')
        self.HP = BOSS_SHIP_00_HP
        self.tmpHP = self.HP
        self.type = 'BossShip00'
        self.lock = False

        #Collision Animation
        self.CollsionImage = load_image('TSData/boss3_collision2.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.Image
        del self.HP
        del self.tmpHP
        del self.PosVector
        del self.type
        del self.lock

        del self.CollsionImage

    def MovingState_Stop(self):
        self.MovingVector = array([[0, 0]], dtype=float32)
    def MovingState_Right(self):
        self.MovingVector = array([[1, 0]], dtype=float32)
    def MovingState_Left(self):
        self.MovingVector = array([[-1, 0]], dtype=float32)
    def MovingState_Forward(self):
        self.MovingVector = array([[0, -1]], dtype=float32)
    def MovingState_Back(self):
        self.MovingVector = array([[0, 1]], dtype=float32)

    def Update(self, frame_time):
        MoveDistance = BOSS_SHIP_00_SPEED * frame_time
        self.PosVector += self.MovingVector * MoveDistance

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

        # 해당 EnemyShip은 특별한 움직임이 있다.
        if (self.y < 550 and self.lock == False):
            self.Moving_State[BOSS_SHIP_00_MOVING_RIGHT](self)
            self.lock = True

        if (self.x > 450):
            self.Moving_State[BOSS_SHIP_00_MOVING_LEFT](self)

        if (self.x < 100):
            self.Moving_State[BOSS_SHIP_00_MOVING_RIGHT](self)

    Moving_State = {
        BOSS_SHIP_00_MOVING_STOP : MovingState_Stop,
        BOSS_SHIP_00_MOVING_RIGHT : MovingState_Right,
        BOSS_SHIP_00_MOVING_LEFT : MovingState_Left,
        BOSS_SHIP_00_MOVING_FORWARD : MovingState_Forward,
        BOSS_SHIP_00_MOVING_BACK : MovingState_Back,
    }

    def Draw(self):
        #hp가 깍이는것은 충돌했다는걸 의미하므로 충돌된 이미지를 렌더링한다.
        if (self.tmpHP != self.HP):
            self.CollsionImage.clip_draw_to_origin(0, 0, 260, 365, self.x, self.y, self.w, self.h)
        elif(self.tmpHP == self.HP):
            self.Image.clip_draw_to_origin(0, 0, 260, 365, self.x, self.y, self.w, self.h)
        self.tmpHP = self.HP

