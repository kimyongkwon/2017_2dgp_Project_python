from stdafx import *
from Sound.Sound import *

#########################################################################################
##
class Ship:
    def GetBulletPos(self):
        return (self.x + self.w / 2), (self.y + self.h / 2)

    def GetBoundingBox(self):
        return self.x, self.y, self.x + self.w, self.y + self.h

    def DrawBoundingBox(self):
        draw_rectangle(*self.GetBoundingBox())

    def CheckCollision(self, ObjectType):

        other_left, other_bottom, other_right, other_top = ObjectType.GetBoundingBox()
        my_left, my_bottom, my_right, my_top = self.GetBoundingBox()

        if my_left > other_right : return False
        if my_right < other_left : return False
        if my_top < other_bottom : return False
        if my_bottom > other_top : return False

        return True

    def CollisionAnimation(self):
        pass

    def DeleteShip(self):
        if (self.y > HEIGHT + BOMB_SHIP_HEIGHT):
            print('ship_delete')
            return True
        return False

    def DeleteEnemyShip(self):
        if self.y + 100< 0:
            print('Enemyship_delete')
            return True
        return False