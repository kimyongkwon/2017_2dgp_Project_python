from Ship.Ship import *
from Bullet.PlayerBullet import *

#########################################################################################
##
class MyShip(Ship):
    image = None

    WeaponLV= 1

    #플레이어의 위치와 방향을 벡터로 표현하였다.
    PosVector = array( [[200, 200]] , dtype = float32)
    RightVector = array( [[1,0]] , dtype = float32)
    UpVector = array( [[0,1]] , dtype = float32)

    Key = {
        UP_KEY : 'up',
        DOWN_KEY : 'down',
        RIGHT_KEY : 'right',
        LEFT_KEY : 'left',
        SHOT_KEY : 'shot'
    }

    Prev_Shot_Button = False

    def __init__(self):
        if (MyShip.image == None):
            MyShip.image = load_image('TSData/Player1.png')
        self.x , self.y = 200 , 200
        self.w , self.h = 50, 50 #캐릭터사이즈
        self.centerX, self.centerY = self.x + self.w/2 , self.y + self.h/2
        self.BulletLV1 = []
        self.BulletLV2 = []
        self.BulletLV3 = []
        self.Weapons = [self.BulletLV1, self.BulletLV2, self.BulletLV3]
        self.EnergyGauge = 0
        self.EnergyBullet = None
        self.BombBullet = []
        self.BombShip = None
        self.Score = 0
        self.GameOver = False
        self.Congratulation = False

        self.RightSubShip = None
        self.LeftSubShip = None

    def __del__(self):
        del self.x, self.y, self.w, self.h
        del self.centerX, self.centerY
        del self.BulletLV1, self.BulletLV2, self.BulletLV3
        del self.Weapons, self.EnergyGauge, self.EnergyBullet
        del self.BombBullet, self.BombShip
        del self.RightSubShip, self.LeftSubShip
        del self.GameOver
        del self.Congratulation
        del MyShip.image
        del MyShip.PosVector
        del MyShip.RightVector
        del MyShip.UpVector

    #EnemyShow클래스에서 PlayerBullet과 EnemyShip이 충돌하면 각자 다르게 점수가 환산됀다
    #state는 단순한 피격인지 아니면 적객체가 부셔지냐
    def AddScore(self, EnemyShip, state):
        if (EnemyShip.type == 'EnemyShip00'): self.Score += 100 * state
        if (EnemyShip.type == 'EnemyShip01'): self.Score += 150 * state
        if (EnemyShip.type == 'EnemyShip02'): self.Score += 200 * state
        if (EnemyShip.type == 'EnemyShip03'): self.Score += 250 * state
        if (EnemyShip.type == 'BossShip00'): self.Score += 500 * state

#keyEvent관련
    def Key_Event(self, event):
        if (event.type == SDL_KEYDOWN):
            if (event.key == SDLK_UP): MyShip.Key[UP_KEY] = True
            if (event.key == SDLK_DOWN): MyShip.Key[DOWN_KEY] = True
            if (event.key == SDLK_RIGHT): MyShip.Key[RIGHT_KEY] = True
            if (event.key == SDLK_LEFT): MyShip.Key[LEFT_KEY] = True
            if (event.key == SDLK_SPACE): MyShip.Key[SHOT_KEY] = True
            if (event.key == SDLK_a): self.WeaponUpgrade()
            if (event.key == SDLK_z): self.BombShot()

        elif (event.type == SDL_KEYUP):
            if (event.key == SDLK_UP): MyShip.Key[UP_KEY] = False
            if (event.key == SDLK_DOWN): MyShip.Key[DOWN_KEY] = False
            if (event.key == SDLK_RIGHT): MyShip.Key[RIGHT_KEY] = False
            if (event.key == SDLK_LEFT): MyShip.Key[LEFT_KEY] = False
            if (event.key == SDLK_SPACE): MyShip.Key[SHOT_KEY] = False

    def GetBoundingBox(self):
        return self.x + 10, self.y + 10, self.x + self.w - 10, self.y + self.h - 10

#bullet관련
        '''
        스페이스 버튼을 한번 누르면 Bullet이 한발만 발사되도록 하였다.
        플레이어가 스페이스버튼을 살짝눌렀다고 해도 게임에서는 한프레임의 버튼의 상태를 
        확인하기 떄문에 스페이스버튼을 한번누르면 여러발이 나갈 수 있다.
        그렇기 때문에 이전에 버튼(Prev_Shot_Button)상태를 확인하는 변수가 필요하다.
        이전에 버튼(Prev_Shot_Button)이 눌려있지않고 현재 스페이스버튼이 눌려있다면
        BuildBullet함수에서 Bullet생성한다.
        '''


    def ShotUpdate(self):
        if (MyShip.Prev_Shot_Button == False and MyShip.Key[SHOT_KEY] == True):
            self.BuildBullet(MyShip.WeaponLV)
        self.ReadyEnergyShot()
        MyShip.Prev_Shot_Button = MyShip.Key[SHOT_KEY]

    def BuildBullet(self, WeaponLevel):
        if (WeaponLevel == 1 or WeaponLevel == 2 or WeaponLevel == 3):
            #탄환레벨이 1
            self.BulletLV1.append(BulletLV1(self.x + (self.w/2), self.y + (self.h/2)))

        #탄환레벨이 2
        if (WeaponLevel == 2 or WeaponLevel == 3):
            #왼쪽탄환생성
            self.BulletLV2.append(BulletLV2( (self.x + (self.w/2) - 30) , (self.y + (self.h/2)) - 30) )
            #오른쪽탄환생성
            self.BulletLV2.append(BulletLV2( (self.x + (self.w/2) + 30), (self.y + (self.h / 2)) - 30) )

        #탄환레벨이 3
        if (WeaponLevel == 3):
            #왼쪽탄환
            self.BulletLV3.append(BulletLV3( self.x + (self.w/2), self.y + (self.h/2) , LEFT_DIRECTION))
            #오른쪽탄환
            self.BulletLV3.append(BulletLV3( self.x+ (self.w / 2), self.y + (self.h / 2), RIGHT_DIRECTION))

    def ReadyEnergyShot(self):
        if(MyShip.Key[SHOT_KEY] == True):
            if (self.EnergyGauge != MAX_ENERGY_GAUGE):
                self.EnergyGauge += 1
        if (MyShip.Key[SHOT_KEY] == False):
            if (self.EnergyGauge == MAX_ENERGY_GAUGE):
                Sounds('energy')
                self.BuildEnergyBullet()
                self.EnergyGauge = 0
            else:
                self.EnergyGauge = 0

    def BuildEnergyBullet(self):
        self.EnergyBullet = EnergyBullet(self.x + (self.w/2), self.y + (self.h/2))

    def BombShot(self):
        #폭발 효과를 만들어낸다.
        if (self.BombShip == None):
            self.BombShip = BombShip()
            self.BombBullet.append(BombBullet(50,100))
            self.BombBullet.append(BombBullet(400,350))
            self.BombBullet.append(BombBullet(50,600))

    def WeaponUpgrade(self):
        if (MyShip.WeaponLV < 3):
            MyShip.WeaponLV += 1
            if (MyShip.WeaponLV == 2):
                self.RightSubShip = SubShip()
                self.LeftSubShip = SubShip()
            if (MyShip.WeaponLV == 1):
                if (self.RightSubShip != None and self.LeftSubShip != None):
                    self.RightSubShip = None
                    self.LeftSubShip = None

    def WeaponDowngrade(self):
        if (MyShip.WeaponLV > 1):
            if (MyShip.WeaponLV == 2):
                if (self.RightSubShip != None and self.LeftSubShip != None):
                    self.RightSubShip = None
                    self.LeftSubShip = None
            MyShip.WeaponLV -= 1

#move관련
    def MoveUpdate(self,frame_time):
        MoveDistance = PLAYER_SPEED * frame_time
        if (MyShip.Key[UP_KEY] == True and (self.y + self.h) < HEIGHT):
            MyShip.PosVector += MyShip.UpVector * MoveDistance
        if (MyShip.Key[DOWN_KEY] == True and self.y > 0 ):
            MyShip.PosVector -= MyShip.UpVector * MoveDistance
        if (MyShip.Key[RIGHT_KEY] == True and (self.x + self.w) < WIDTH):
            MyShip.PosVector += MyShip.RightVector * MoveDistance
        if (MyShip.Key[LEFT_KEY] == True and self.x > 0):
            MyShip.PosVector -= MyShip.RightVector * MoveDistance

    def update(self,frame_time):
        self.x = MyShip.PosVector[0][0]
        self.y = MyShip.PosVector[0][1]

        if (self.WeaponLV != 1):
            self.RightSubShip.Update( self.x + (self.w/2) - 30, self.y + (self.h/2) - 30 )
            self.LeftSubShip.Update( self.x + (self.w/2) + 30, self.y + (self.h/2) - 30 )

        self.MoveUpdate(frame_time)
        self.ShotUpdate()

    def draw(self):
        if (self.WeaponLV != 1):
            self.RightSubShip.Draw()
            self.LeftSubShip.Draw()
        MyShip.image.clip_draw_to_origin(0, 0, 100, 100, self.x, self.y, self.w, self.h)

#########################################################################################
##
class SubShip:
    def __init__(self):
        self.w, self.h = 20, 20
        self.x, self.y = 0,0
        self.SubPlayerImage = load_image('TSData/Player2.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.SubPlayerImage

    def Update(self, PlayerX, PlayerY):
        self.x = PlayerX - (self.w / 2)
        self.y = PlayerY - (self.h / 2)

    def Draw(self):
        self.SubPlayerImage.clip_draw_to_origin(0, 0, 100, 100, self.x, self.y, self.w, self.h)

#########################################################################################
##
class BombShip(Ship):
    def __init__(self):
        self.x, self.y = WIDTH/2 - 240, -800
        self.w, self.h = BOMB_SHIP_WIDTH, BOMB_SHIP_HEIGHT
        self.Image = load_image('TSData/BombShip.png')
        self.ReadyBombEffect = False
    def __del__(self):
        del self.x , self.y
        del self.w, self.h
        del self.Image
        del self.ReadyBombEffect
    def Update(self, frame_time):
        MoveDistance = BOMB_SHIP_SPEED * frame_time
        self.y += MoveDistance
        if (self.y > BOMB_EFFECT_POINT):
            self.ReadyBombEffect = True
    def Draw(self):
        self.Image.clip_draw_to_origin(0,0,182,130,self.x, self.y, self.w, self.h)







