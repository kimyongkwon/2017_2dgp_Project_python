from stdafx import *

#########################################################################################
##
class Bullet:
    def GetBoundingBox(self):
        return self.x, self.y, self.x + self.w, self.y + self.h

    def DrawBoundingBox(self):
        draw_rectangle(*self.GetBoundingBox())

    def CheckCollision(self, ObjectType):
        other_left, other_bottom, other_right, other_top = ObjectType.GetBoundingBox()
        my_left, my_bottom, my_right, my_top = self.GetBoundingBox()

        if my_left > other_right : return False
        if my_right < other_left : return False
        if my_top < other_bottom : return False
        if my_bottom > other_top : return False

        return True

    def DeleteBullet(self, MaxX, MinX, MaxY, MinY):
        if (self.x > MaxX or self.x < 0 or self.y > MaxY or self. y  < -20):
            #print("Bullet_delete")
            return True
        return False






