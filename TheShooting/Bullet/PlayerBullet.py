from Bullet.Bullet import *


##########################################################################################
##
class BulletLV1(Bullet):
    image = None

    ShotVector = array([[0, 1]], dtype=float32)

    def __init__(self, PlayerX, PlayerY):
        # PosVector의 인자x,y를 나타내는 변수
        self.x, self.y = 0, 0
        self.w, self.h = 15,15
        self.PosVector = array([[0, 0]], dtype=float32)
        self.PosVector[0][0] = PlayerX - (self.w/2)
        self.PosVector[0][1] = PlayerY - (self.h/2)

        if (BulletLV1.image == None):
            BulletLV1.image = load_image('TSData/Bullet3.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.PosVector

    def Update(self, frame_time, BulletSpeed = BULLET_SPEED):
        MoveDistance = BulletSpeed * frame_time
        self.PosVector += BulletLV1.ShotVector * MoveDistance

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

    def Draw(self):
        BulletLV1.image.clip_draw_to_origin(0, 0, 37, 37, self.x, self.y, self.w, self.h)

##########################################################################################
##
class BulletLV2(Bullet):
    image = None
    MoveDistance = 0

    ShotVector = array([[0, 1]], dtype=float32)

    def __init__(self, PlayerX, PlayerY):
        # PosVector의 인자x,y를 나타내는 변수
        self.x, self.y = 0, 0
        self.w, self.h = 15, 15
        self.PosVector = array([[0, 0]], dtype=float32)
        self.PosVector[0][0] = PlayerX - (self.w / 2)
        self.PosVector[0][1] = PlayerY - (self.h / 2)

        if (BulletLV2.image == None):
            BulletLV2.image = load_image('TSData/Bullet3.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.PosVector

    def Update(self, frame_time, BulletSpeed = BULLET_SPEED):
        BulletLV2.MoveDistance = BulletSpeed * frame_time
        self.PosVector += BulletLV2.ShotVector * BulletLV2.MoveDistance

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

    def Draw(self):
        self.image.clip_draw_to_origin(0, 0, 37, 37, self.x, self.y, self.w, self.h)


#########################################################################################
##
class BulletLV3(Bullet):
    image = None
    MoveDistance = 0

    RightShotVector = array([[0.5, 1]], dtype=float32)
    LeftShotVector = array([[-0.5, 1]], dtype=float32)

    def __init__(self, PlayerX, PlayerY, Direction):
        # PosVector의 인자x,y를 나타내는 변수
        self.x, self.y = 0, 0
        self.w, self.h = 15, 15
        self.PosVector = array([[0, 0]], dtype=float32)
        self.PosVector[0][0] = PlayerX - (self.w / 2)
        self.PosVector[0][1] = PlayerY - (self.h / 2)

        self.Direction = Direction
        if (BulletLV3.image == None):
            BulletLV3.image = load_image('TSData/Bullet3.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.PosVector

    def Update(self, frame_time, BulletSpeed = BULLET_SPEED):
        BulletLV3.MoveDistance = BulletSpeed * frame_time

        if (self.Direction == LEFT_DIRECTION):
            self.PosVector += BulletLV3.LeftShotVector * BulletLV3.MoveDistance
        if (self.Direction == RIGHT_DIRECTION):
            self.PosVector += BulletLV3.RightShotVector * BulletLV3.MoveDistance

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

        #self.y += BulletLV3.MoveDistance

    def Draw(self):
        self.image.clip_draw_to_origin(0, 0, 37, 37, self.x, self.y, self.w, self.h)


##########################################################################################
##
class EnergyBullet:
    image = None

    ShotVector = array([[0, 1]], dtype=float32)

    def __init__(self, PlayerX, PlayerY):
        # PosVector의 인자x,y를 나타내는 변수
        self.x, self.y = 0, 0
        self.w, self.h = 250,1000
        self.PosVector = array([[0, 0]], dtype=float32)
        self.PosVector[0][0] = PlayerX - (self.w / 2)
        self.PosVector[0][1] = PlayerY - (self.h / 2) + 500
        self.DeleteTime = 0

        if (EnergyBullet.image == None):
            EnergyBullet.image = load_image('TSData/EnergyBullet2.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.PosVector
        del self.DeleteTime

    def GetBoundingBox(self):
        return self.x, self.y, self.x + self.w, self.y + self.h

    def DrawBoundingBox(self):
        draw_rectangle(*self.GetBoundingBox())

    def CheckCollision(self, ObjectType):
        other_left, other_bottom, other_right, other_top = ObjectType.GetBoundingBox()
        my_left, my_bottom, my_right, my_top = self.GetBoundingBox()

        if my_left > other_right : return False
        if my_right < other_left : return False
        if my_top < other_bottom : return False
        if my_bottom > other_top : return False

        return True

    def Update(self, frame_time, BulletSpeed = BULLET_SPEED):
        #MoveDistance = BulletSpeed * frame_time
        #self.PosVector += EnergyBullet.ShotVector * MoveDistance
        self.DeleteTime += 1

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

    def Draw(self):
        EnergyBullet.image.clip_draw_to_origin(0, 0, 390, 440, self.x, self.y, self.w, self.h)


##########################################################################################
##
#BombShip을 소환했을때 옆에 터지는 효과들을 표현하는 Bullet이다.
class BombBullet(Bullet):
    image = None
    BombShip = None

    def __init__(self, PosX, PosY):
        self.x = PosX
        self.y = PosY
        self.frame = 0
        self.BombTime = 0
        if (BombBullet.image == None):
            BombBullet.image = load_image('TSData/Bomb_Animation2.png')

    def __del__(self):
        del self.x, self.y
        del self.frame
        del self.BombTime

    def Update(self):
        Delay = 5
        if (self.BombTime < Delay):
            self.BombTime += 1
            if (self.BombTime >= Delay):
                self.frame = (self.frame + 1) % 81
                self.BombTime = 0

    def Draw(self):
        BombBullet.image.clip_draw_to_origin(self.frame * 100, 100 * (self.frame % 9),
                                              100, 100, self.x, self.y, 300, 300)

    def DeleteBombBullet(self):
        if (self.frame > 10):
            print("bomb_delete")
            return True
        return False

