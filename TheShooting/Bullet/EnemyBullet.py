from Bullet.Bullet import *

##########################################################################################
##
class Enemy00Bullet(Bullet):
    image = None

    def __init__(self, EnemyX, EnemyY):
        # PosVector의 인자x,y를 나타내는 변수
        self.x, self.y = 0, 0
        self.w, self.h = 15, 15
        self.PosVector = array([[0, 0]], dtype=float32)
        self.PosVector[0][0] = EnemyX - (self.w / 2)
        self.PosVector[0][1] = EnemyY - (self.h / 2)
        self.ShotVector = array([[0, -1]], dtype=float32)

        if (Enemy00Bullet.image == None):
            Enemy00Bullet.image = load_image('TSData/Bullet2.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.PosVector

    def Update(self, frame_time):
        MoveDistance = ENEMY_SHIP_00_BULLET_SPEED * frame_time
        self.PosVector += self.ShotVector * MoveDistance

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

    def Draw(self):
        Enemy00Bullet.image.clip_draw_to_origin(0, 0, 37, 37, self.x, self.y, self.w, self.h)


##########################################################################################
## EnemyShip01과 EnemyShip02가 쓴다.
class Enemy01Bullet(Bullet):
    image = None

    def __init__(self, EnemyX, EnemyY, VectorX, VectorY):
        # PosVector의 인자x,y를 나타내는 변수
        self.x, self.y = 0, 0
        self.w, self.h = 15, 15
        self.PosVector = array([[0, 0]], dtype=float32)
        self.PosVector[0][0] = EnemyX - (self.w / 2)
        self.PosVector[0][1] = EnemyY - (self.h / 2)
        self.ShotVector = array([[VectorX, VectorY]], dtype=float32)


        if (Enemy01Bullet.image == None):
            Enemy01Bullet.image = load_image('TSData/Bullet2.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.PosVector
        del self.ShotVector

    def Update(self, frame_time):
        MoveDistance = ENEMY_SHIP_01_BULLET_SPEED * frame_time
        self.PosVector += self.ShotVector * MoveDistance

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

    def Draw(self):
        Enemy01Bullet.image.clip_draw_to_origin(0, 0, 37, 37, self.x, self.y, self.w, self.h)

##########################################################################################
## 방향탄
class Enemy02Bullet(Bullet):
    image = None

    def __init__(self, EnemyX, EnemyY, VectorX, VectorY):
           # PosVector의 인자x,y를 나타내는 변수
        self.x, self.y = 0, 0
        self.w, self.h = 15,15
        self.PosVector = array([[0, 0]], dtype=float32)
        self.PosVector[0][0] = EnemyX - (self.w/2)
        self.PosVector[0][1] = EnemyY - (self.h/2)
        self.ShotVector = array( [VectorX,VectorY] , dtype=float32)#정규화

        if (Enemy02Bullet.image == None):
            Enemy02Bullet.image = load_image('TSData/Bullet2.png')

    def __del__(self):
        del self.x, self.y
        del self.w, self.h
        del self.PosVector
        del self.ShotVector

    def Update(self, frame_time):
        MoveDistance = ENEMY_SHIP_03_BULLET_SPEED * frame_time
        self.PosVector += self.ShotVector * MoveDistance

        self.x = self.PosVector[0][0]
        self.y = self.PosVector[0][1]

    def Draw(self):
        Enemy02Bullet.image.clip_draw_to_origin(0, 0, 37, 37, self.x, self.y, self.w, self.h)