from stdafx import *

class BaseSound:
    def __init__(self):
        pass
    def stop(self):
        self.bgm.stop()
    def pause(self):
        self.bgm.pause()

#########################################################################
#배경음악 사운드(mp3,ogg)
class MainSound:
    def __init__(self):
        self.bgm = load_music('TSData/Main_sound.mp3')
        self.bgm.set_volume(20)
        self.bgm.repeat_play()
    def __del__(self):
        del self.bgm

class InGameSound:
    def __init__(self):
        self.bgm = load_music('TSData/ingame_sound2.mp3')
        self.bgm.set_volume(20)
        self.bgm.repeat_play()
    def __del__(self):
        del self.bgm

#########################################################################
#이펙트 사운드(wav)
class EnemyDownSound:
    def __init__(self):
        self.bgm = load_wav('TSData/enemydown_sound2.wav')
        self.bgm.set_volume(10)
        self.bgm.play(1)

    def __del__(self):
        #('del sound')
        del self.bgm

class EnemyDamagedSound:
    def __init__(self):
        self.bgm = load_wav('TSData/enemydamaged_sound.wav')
        self.bgm.set_volume(10)
        self.bgm.play(1)
    def __del__(self):
        del self.bgm

class PlayerEnergyShotSound:
    def __init__(self):
        self.bgm = load_wav('TSData/energyshot_sound.wav')
        self.bgm.set_volume(10)
        self.bgm.play(1)

    def __del__(self):
        del self.bgm

class ItemGetSound:
    def __init__(self):
        self.bgm = load_wav('TSData/getitem_sound.wav')
        self.bgm.set_volume(80)
        self.bgm.play(1)

    def __del__(self):
        #print('del sound')
        del self.bgm

class PlayerBombSound:
    def __init__(self):
        self.bgm = load_wav('TSData/bomb_sound.wav')
        self.bgm.set_volume(30)
        self.bgm.play(1)

    def __del__(self):
        #print('del sound')
        del self.bgm

#이펙트 사운드를 관리하는 함수
def Sounds(type):
    #전역변수로 선언해야지만 어디서든 쓰일 수 있다.
    global DownSound
    global DamagedSound
    global EnergyShotSound
    global ItemSound
    global BombSound

    DownSound = None
    DamagedSound = None
    EnergyShotSound = None
    ItemSound = None
    BombSound = None

    if (type == 'damaged'):
        if (DamagedSound != None): del DamagedSound
        DamagedSound = EnemyDamagedSound()

    if (type == 'down'):
        if (DownSound != None): del DownSound
        DownSound = EnemyDownSound()

    if (type == 'energy'):
        if (EnergyShotSound != None): del EnergyShotSound
        EnergyShotSound = PlayerEnergyShotSound()

    if (type == 'item'):
        if (ItemSound  != None): del ItemSound
        ItemSound = ItemGetSound()

    if (type == 'bomb'):
        if (BombSound  != None): del BombSound
        BombSound = PlayerBombSound()






