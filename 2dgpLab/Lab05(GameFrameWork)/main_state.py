from pico2d import *

import title_state
import game_framework
import random

grass = None
team = None

class Grass:
    def __init__(self):
        self.image = load_image('../LabData/grass.png')
    def draw(self):
        self.image.draw(400,30)

class Boy:
    def __init__(self):
        self.x, self.y = random.randint(100,700), 90
        self.frame = random.randint(0,7)
        self.image = load_image('../LabData/run_animation.png')
        self.turn = True

    def update(self):
        self.frame = (self.frame + 1) % 8
        self.x += 5

    def draw(self):
        self.image.clip_draw(self.frame * 100, 0, 100, 100, self.x , self.y)

def enter():
    global grass
    global team
    team = [Boy() for i in range(11)]
    grass = Grass()

def exit():
    global grass
    global team
    del(team)
    del(grass)

def handle_events():
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            game_framework.change_state(title_state)

def update():
        for boy in team:
            boy.update()
        delay(0.05)

def draw():
    clear_canvas()
    grass.draw()
    for boy in team:
        boy.draw()
    update_canvas()