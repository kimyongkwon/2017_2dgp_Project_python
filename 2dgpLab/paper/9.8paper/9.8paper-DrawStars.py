import turtle

###########
def draw():
    turtle.penup()
    #중심과 별은 100만큼 떨어져있다
    turtle.forward(100)

    #별의 한 별은 30이다.
    turtle.pendown()
    turtle.forward(30)
    turtle.right(144)
    turtle.forward(30)
    turtle.right(144)
    turtle.forward(30)
    turtle.right(144)
    turtle.forward(30)
    turtle.right(144)
    turtle.forward(30)

    turtle.right(144)

    turtle.penup()

    #중심과 별은 100만큼 떨어져있다    
    turtle.backward(100)
############    


############    
def star(n):
    angle = (360 / n)
    for i in range(0,n):
        turtle.left(angle)
        draw()
############        

turtle = turtle.Turtle()
turtle.shape('turtle')

#그리고 싶은 별의 갯수를 입력받는다.
n = int(input("별을 몇개 그릴건가요? : "))

star(n)
                










