from pico2d import *
import random

class Grass:
    def __init__(self):
        self.image = load_image('../../LabData/grass.png')
    def draw(self):
        self.image.draw(400,30)

class Boy:
    def __init__(self):
        self.x, self.y = random.randint(100,700), random.randint(100,600)
        self.frame = random.randint(0,7)
        self.image = load_image('../../LabData/run_animation.png')
        self.select = False

    def update(self):
        self.frame = (self.frame + 1) % 8
        if self.select == False:
            self.x += 5

    def draw(self):
        self.image.clip_draw(self.frame * 100, 0, 100, 100, self.x , self.y)

boy_keys = { SDLK_0 : 0, SDLK_1 : 1 , SDLK_2 : 2 , SDLK_3 : 3 , SDLK_4 : 4,
             SDLK_5 : 5, SDLK_6 : 6 , SDLK_7 : 7 , SDLK_8 : 8 , SDLK_9 : 9, SDLK_a : 10}

def handle_events():
    global running
    global x
    global y
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            running = False
        elif event.type == SDL_MOUSEMOTION:
            for boy in team:
                if boy.select == True:
                    boy.x, boy.y = event.x, 600 - event.y
                elif boy.select == False:
                    x, y = event.x, 600 - event.y
        elif event.type == SDL_KEYDOWN:
            for boy in team:
                boy.select = False
            if event.key == SDLK_ESCAPE:
                running = False

                # 방법1
                """
                elif event.key == SDLK_0:
                    team[0].select = True
                elif event.key == SDLK_1:
                    team[1].select = True
                elif event.key == SDLK_2:
                    team[2].select = True
                elif event.key == SDLK_3:
                    team[3].select = True
                elif event.key == SDLK_4:
                    team[4].select = True
                elif event.key == SDLK_5:
                    team[5].select = True
                elif event.key == SDLK_6:
                    team[6].select = True
                elif event.key == SDLK_7:
                    team[7].select = True
                elif event.key == SDLK_8:
                    team[8].select = True
                elif event.key == SDLK_9:
                    team[9].select = True
                elif event.key == SDLK_a:
                    team[10].select = True
                """

            #방법2
            for keys in boy_keys:
                if event.key == keys:
                    team[boy_keys[keys]].select = True


open_canvas()

#boy = Boy()

team = [Boy() for i in range(11)]

grass = Grass()

running = True;

while running:
    handle_events()
    for boy in team:
        boy.update()
    clear_canvas()
    grass.draw()
    for boy in team:
        boy.draw()
    update_canvas()
    delay(0.05)

close_canvas()
