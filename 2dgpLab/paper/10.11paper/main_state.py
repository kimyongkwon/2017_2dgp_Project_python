from pico2d import *

import title_state
import game_framework
import random
import ai_boy
import ai_boy_IQ10
import numbers

grass = None
team = None
selectIndex = 0

MAX_TEAM = 1000

class Grass:
    def __init__(self):
        self.image = load_image('../../LabData/grass.png')
    def draw(self):
        self.image.draw(400,30)

def enter():
    global grass
    global team
    global boyNumber
    team = [ai_boy_IQ10.Boy() for i in range(0,MAX_TEAM)]
    grass = Grass()

def exit():
    global grass
    global team
    del(team)
    del(grass)

def handle_events():
    global selectIndex
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            game_framework.change_state(title_state)
        elif event.type == SDL_MOUSEMOTION:
            for boy in team:
                if boy.select == True:
                    boy.x, boy.y = event.x , 600 - event.y
                elif boy.select == False:
                    x, y = event.x , 600 - event.y

        elif event.type == SDL_KEYDOWN:
            for boy in team:
                boy.select = False
            if event.key == SDLK_UP:
                team[selectIndex].select = True
                selectIndex += 1
                if selectIndex == MAX_TEAM:
                    selectIndex = 0
            elif event.key == SDLK_DOWN:
                team[selectIndex].select = True
                selectIndex -= 1
                if selectIndex == -1:
                    selectIndex = MAX_TEAM-1
            print(selectIndex + 1)

def update():
        for boy in team:
            boy.update()
        delay(0.05)

def draw():
    clear_canvas()
    grass.draw()
    for boy in team:
        boy.draw()
    numbers.draw(selectIndex+1,740,540)
    update_canvas()