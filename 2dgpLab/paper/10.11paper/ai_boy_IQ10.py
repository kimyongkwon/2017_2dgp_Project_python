from pico2d import *

import time
import random

class Boy:
    image = None

    LEFT_RUN , RIGHT_RUN , LEFT_STAND , RIGHT_STAND = 0,1,2,3

    def __init__(self):
        self.x, self.y = random.randint(100,700), 90
        self.frame = random.randint(0,7)
        self.dir = 1
        self.state = Boy.RIGHT_RUN
        self.run_frames = 0
        self.select = False
        self.time = time.time()
        self.elapsed = 0

        #클래스 변수는 Boy 이런식으로 꼭 써야한다.
        if Boy.image == None:
           Boy.image = load_image('../../LabData/animation_sheet.png')

    def handle_left_run(self):
        self.x -= 5
        if self.x < 0:
            self.state = Boy.RIGHT_RUN
            self.x = 0
        if (self.elapsed > 5.0):
            self.time = time.time()
            self.state = Boy.LEFT_STAND

    def handle_left_stand(self):
        if (self.elapsed > 5.0):
            self.time = time.time()
            self.state = Boy.LEFT_RUN

    def handle_right_run(self):
        self.x += 5
        if self.x > 800:
            self.state = Boy.LEFT_RUN
            self.x = 800
        if (self.elapsed > 5.0):
            self.time = time.time()
            self.state = Boy.RIGHT_STAND

    def handle_right_stand(self):
        if (self.elapsed > 5.0):
            self.time = time.time()
            self.state = Boy.RIGHT_RUN

    handle_state = {
        LEFT_RUN : handle_left_run,
        RIGHT_RUN : handle_right_run,
        LEFT_STAND : handle_left_stand,
        RIGHT_STAND : handle_right_stand
    }

    def update(self):
        self.frame = (self.frame + 1 ) % 8
        self.handle_state[self.state] (self)
        self.elapsed = time.time() - self.time

    def draw(self):
        self.image.clip_draw(self.frame * 100, self.state * 100, 100, 100, self.x , self.y)