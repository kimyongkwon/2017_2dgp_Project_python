from pico2d import *
import json
import time
import game_framework
import random

canvasWidth = 480
canvasHeight = 272

class TileBackGround:
    def __init__(self, filename):
        f = open(filename)
        self.map = json.load(f)
        self.x = 0
        self.y = 0
        image_filename = self.map['tileset'][0]['image']
        self.image = load_image(image_filename)

    def draw(self):
        tile_per_line = self.map['width']
        data = self.map['laysers'][0]['data']
        tileset = self.map['tileset'][0]
        tile_width = tileset['tilewidth']
        tile_height = tileset['tileheight']
        margin = tileset['margin']
        spacing = tileset['spacing']
        columns = tileset['columns']
        dx, dy = 0, 0
        desty = dy
        for y in range(5):
            destx = dx
            for x in range(10):
                index = y * tile_per_line + x
                tile = data
                tx = tile % columns
                ty = tile // columns #나눗셈을 하고 소수점을 버린다.
                srcx = margin + tx * (tile_width + spacing)
                srcy = margin + ty * (tile_height + spacing)
                self.image.clip_draw(srcx, srcy, tile_width, tile_height, destx, desty)
                desty += tile_width
            desty += tile_height


team = None
selectIndex = 0


def enter():
    global grass
    global team
    grass = Grass()

def exit():
    global grass
    global team
    del(team)
    del(grass)

def handle_events():
    global selectIndex
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            game_framework.change_state(title_state)
        elif event.type == SDL_KEYDOWN and (event.key == SDLK_UP or event.key == SDLK_DOWN):
            for boy in team:
                boy.select = False
            if event.key == SDLK_UP:
                selectIndex += 1
                if selectIndex == 5:
                    selectIndex = 0
                team[selectIndex].select = True
            elif event.key == SDLK_DOWN:
                selectIndex -= 1
                if selectIndex == -1:
                    selectIndex = 5 - 1
                team[selectIndex].select = True
        else:
            team[selectIndex].handle_event(event)

def update():
    global start_time
    global frame_time

    frame_time = get_time() - start_time

    for boy in team:
        boy.update(frame_time)

    start_time += frame_time

    delay(0.1)

def draw():
    clear_canvas()
    grass.draw()
    for boy in team:
        boy.draw()
    numbers.draw(selectIndex + 1, 740, 540)
    update_canvas()