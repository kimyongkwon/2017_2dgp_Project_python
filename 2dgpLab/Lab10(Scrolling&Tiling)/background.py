from pico2d import *

class BackGround:
    PIXEL_PER_METER = (10.0 / 0.3)  # 10 pixel 30 cm
    SCROLL_SPEED_KMPH = 20.0  # km / hour
    SCROLL_SPEED_MPM = (SCROLL_SPEED_KMPH * 1000.0 / 60.0)
    SCROLL_SPEED_MPS = (SCROLL_SPEED_MPM / 60.0)
    SCROLL_SPEED_PPS = (SCROLL_SPEED_MPS * PIXEL_PER_METER)

    def __init__(self):
        self.left = 0
        self.screen_width = 800
        self.screen_height = 600
        self.speed = 0
        self.image = load_image('../LabData/background.png')

    def draw(self):
        x = int(self.left)
        w = min(self.image.w - x, self.screen_width)
        self.image.clip_draw_to_origin(x, 0, w, self.screen_height, 0, 0)
        self.image.clip_draw_to_origin(0, 0, self.screen_width - w, self.screen_height, w, 0)

    def update(self, frame_time):
        self.speed = 500
        self.left = (self.left + frame_time * self.speed) % self.image.w

    def handle_event(self, event):
        if event.type == SDL_KEYDOWN:
            if event.key == SDLK_LEFT : self.speed -= BackGround.SCROLL_SPEED_PPS
            elif event.key == SDLK_RIGHT: self.speed += BackGround.SCROLL_SPEED_PPS
        if event.type == SDL_KEYUP:
            if event.key == SDLK_LEFT: self.speed += BackGround.SCROLL_SPEED_PPS
            elif event.key == SDLK_RIGHT: self.speed -= BackGround.SCROLL_SPEED_PPS

class TileBackGround:
    def __init__(self, filename, width, height):
        f = open(filename)
        self.map = json.load(f)
        self.x = 0
        self.y = 0
        self.layerIndex = 0
        self.canvasWidth = width
        self.canvasHeight = height
        self.speedX = 0
        self.speedY = 0
        self.image = load_image('../LabData/tile.png')

    def draw(self):
        map_width = self.map['width']
        map_height = self.map['height']
        data = self.map['layers'][self.layerIndex]['data']
        tileset = self.map['tilesets'][0]
        tile_width = tileset['tilewidth']
        tile_height = tileset['tileheight']
        margin = tileset['margin']
        spacing = tileset['spacing']
        columns = tileset['columns']
        rows = - ( -tileset['tilecount'] // columns)

        startx = tile_width // 2 - self.x % tile_width
        starty = tile_height // 2 - self.y % tile_height

        endx = self.canvasWidth + tile_width // 2
        endy = self.canvasHeight + tile_height // 2

        desty = starty
        my = int(self.y // tile_height)

        while(desty < endy):
            destx = startx
            mx = int (self.x // tile_width)

            while(destx < endx):
                index = (map_height - my - 1) * map_width + mx
                tile = data[index]
                tx = (tile -1) % columns
                ty = rows - (tile - 1) // columns - 1
                srcx = margin + tx * (tile_width + spacing)
                srcy = margin + ty * (tile_height + spacing)
                self.image.clip_draw(srcx, srcy, tile_width, tile_height, destx, desty)
                destx += tile_width
                mx += 1
            desty += tile_height
            my += 1

    #def update(self, frametile):
        #self.x