from pico2d import *

import boy
import background

start_time = 0
frame_time = 0

def handle_events():
    global running
    global x
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            running = False
        elif event.type == SDL_MOUSEMOTION:
            x, y = event.x, 600 - event.y
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            running = False
        boy.handle_event(event)
        background.handle_event(event)

def create_world():
    global boy, background
    boy = boy.Boy()
    background = background.BackGround()

def draw(frame_time):
    clear_canvas()
    background.draw()
    boy.draw()
    update_canvas()

def main():
    open_canvas()

    create_world()

    global running
    running = True

    while running:
        global start_time
        global frame_time

        frame_time = get_time() - start_time

        #Game Logic
        handle_events()
        boy.update( frame_time)
        background.update(frame_time)

        # GameRendering
        draw(frame_time)

        start_time += frame_time

        delay(0.05)
    close_canvas()

if __name__ == '__main__':
    main()
