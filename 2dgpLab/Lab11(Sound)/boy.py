from pico2d import *

import random

class Boy:
    PIXEL_PER_METER = (10.0 / 0.3)      # 10 pixel 30 cm
    RUN_SPEED_KMPH = 20.0               # km / hour
    RUN_SPEED_MPM = (RUN_SPEED_KMPH * 1000.0 / 60.0)
    RUN_SPEED_MPS = (RUN_SPEED_MPM / 60.0)
    RUN_SPEED_PPS = (RUN_SPEED_MPS * PIXEL_PER_METER)

    TIME_PER_ACTION = 0.5
    ACTION_PER_TIME = 1.0 / TIME_PER_ACTION
    FRAMES_PER_ACTION = 8

    image = None
    eat_sound = None

    LEFT_RUN , RIGHT_RUN , LEFT_STAND , RIGHT_STAND = 0,1,2,3

    def __init__(self):
        self.x, self.y = random.randint(100,700), 90
        self.frame = random.randint(0,7)
        self.state = Boy.LEFT_STAND
        self.run_frames = 0
        self.stand_frames = 0
        self.select = False
        self.total_frames = 0
        self.distance = 0

        #클래스 변수는 Boy 이런식으로 꼭 써야한다.
        if Boy.image == None:
           Boy.image = load_image('../LabData/animation_sheet.png')

        if Boy.eat_sound == None:
            Boy.eat_sound = load_wav('../LabData/pickup.wav')
            Boy.eat_sound.set_volume(32)

    def eat(self,ball):
        self.eat_sound.play()

    def handle_event(self, event):
        if (event.type, event.key) == (SDL_KEYDOWN, SDLK_LEFT):
            if self.state in (self.RIGHT_STAND, self.LEFT_STAND):
                self.state = self.LEFT_RUN
        elif (event.type, event.key) == (SDL_KEYDOWN, SDLK_RIGHT):
            if self.state in (self.RIGHT_STAND, self.LEFT_STAND):
                self.state = self.RIGHT_RUN
        elif (event.type, event.key) == (SDL_KEYUP, SDLK_LEFT):
            if self.state in (self.LEFT_RUN,):
                self.state = self.LEFT_STAND
        elif (event.type, event.key) == (SDL_KEYUP, SDLK_RIGHT):
            if self.state in (self.RIGHT_RUN,):
                self.state = self.RIGHT_STAND


    def get_bb(self):
        return self.x - 50, self.y - 50, self.x + 50, self.y + 50

    def draw_bb(self):
        draw_rectangle(*self.get_bb())

    def update(self, frame_time):
        self.distance = Boy.RUN_SPEED_PPS * frame_time
        self.total_frames += Boy.FRAMES_PER_ACTION * Boy.ACTION_PER_TIME * frame_time
        self.frame = int(self.total_frames) % 8

        if self.state == self.RIGHT_RUN:
            self.x += (1 * self.distance)

        elif self.state == self.LEFT_RUN:
            self.x += (-1 * self.distance)

    def draw(self):
        self.image.clip_draw(self.frame * 100, self.state * 100, 100, 100, self.x , self.y)