
from pico2d import *

import random

class Boy:
    PIXEL_PER_METER = (10.0 / 0.3)      # 10 pixel 30 cm
    RUN_SPEED_KMPH = 20.0               # km / hour
    RUN_SPEED_MPM = (RUN_SPEED_KMPH * 1000.0 / 60.0)
    RUN_SPEED_MPS = (RUN_SPEED_MPM / 60.0)
    RUN_SPEED_PPS = (RUN_SPEED_MPS * PIXEL_PER_METER)

    TIME_PER_ACTION = 0.5
    ACTION_PER_TIME = 1.0 / TIME_PER_ACTION
    FRAMES_PER_ACTION = 8

    image = None
    eat_sound = None

    LEFT_RUN , RIGHT_RUN , LEFT_STAND , RIGHT_STAND = 0,1,2,3

    def __init__(self):
        self.x, self.y = random.randint(100,700), 90
        self.frame = random.randint(0,7)
        self.state = Boy.LEFT_STAND
        self.run_frames = 0
        self.stand_frames = 0
        self.select = False
        self.total_frames = 0
        self.distance = 0

        #클래스 변수는 Boy 이런식으로 꼭 써야한다.
        if Boy.image == None:
           Boy.image = load_image('../LabData/animation_sheet.png')

        if Boy.eat_sound == None:
            Boy.eat_sound = load_wav('../LabData/pickup.wav')
            Boy.eat_sound.set_volume(32)

    def eat(self,ball):
        self.eat_sound.play()

    def handle_event(self, event):
        if (event.type, event.key) == (SDL_KEYDOWN, SDLK_LEFT):
            if self.state in (self.RIGHT_STAND, self.LEFT_STAND):
                self.state = self.LEFT_RUN
        elif (event.type, event.key) == (SDL_KEYDOWN, SDLK_RIGHT):
            if self.state in (self.RIGHT_STAND, self.LEFT_STAND):
                self.state = self.RIGHT_RUN
        elif (event.type, event.key) == (SDL_KEYUP, SDLK_LEFT):
            if self.state in (self.LEFT_RUN,):
                self.state = self.LEFT_STAND
        elif (event.type, event.key) == (SDL_KEYUP, SDLK_RIGHT):
            if self.state in (self.RIGHT_RUN,):
                self.state = self.RIGHT_STAND


    def get_bb(self):
        return self.x - 50, self.y - 50, self.x + 50, self.y + 50

    def draw_bb(self):
        draw_rectangle(*self.get_bb())

    def update(self, frame_time):
        self.distance = Boy.RUN_SPEED_PPS * frame_time
        self.total_frames += Boy.FRAMES_PER_ACTION * Boy.ACTION_PER_TIME * frame_time
        self.frame = int(self.total_frames) % 8

        if self.state == self.RIGHT_RUN:
            self.x += (1 * self.distance)

        elif self.state == self.LEFT_RUN:
            self.x += (-1 * self.distance)

    def draw(self):
        self.image.clip_draw(self.frame * 100, self.state * 100, 100, 100, self.x , self.y)


#############################

class Ball:
    image = None

    def __init__(self):
        self.x, self.y = random.randint(200, 790), 60
        if Ball.image == None:
            Ball.image = load_image('../LabData/ball21x21.png')

    def update(self, frame_time):
        pass

    def draw(self):
        self.image.draw(self.x, self.y)

    def get_bb(self):
        return self.x - 10, self.y - 10, self.x + 10, self.y + 10

    def draw_bb(self):
        draw_rectangle(*self.get_bb())



class BigBall(Ball):
    image = None

    def __init__(self):
        self.x, self.y = random.randint(100, 700), 500
        self.fall_speed = random.randint(50,120)
        if BigBall.image == None:
            BigBall.image = load_image('../LabData/ball41x41.png')

    def update(self, frame_time):
        self.y -= frame_time * self.fall_speed

    def draw(self):
        self.image.draw(self.x, self.y)

    def get_bb(self):
        return self.x - 20, self.y - 20, self.x + 20, self.y + 20

    def stop(self):
        self.fall_speed = 0

    def draw_bb(self):
        draw_rectangle(*self.get_bb())

######################################


start_time = 0
frame_time = 0

class Grass:
    def __init__(self):
        self.x = 400
        self.y = 30
        self.image = load_image('../LabData/grass.png')
        self.bgm = load_music('../LabData/football.mp3')
        self.bgm.set_volume(64)
        self.bgm.repeat_play()

    def draw(self):
        self.image.draw(self.x, self.y)
    def get_bb(self):
        return self.x - 400, self.y - 30, self.x + 400, self.y + 30
    def draw_bb(self):
        draw_rectangle(*self.get_bb())

def handle_events():
    global running
    global x
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            running = False
        elif event.type == SDL_MOUSEMOTION:
            x, y = event.x, 600 - event.y
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            running = False
        boy.handle_event(event)

def create_world():
    global boy, grass, balls, big_balls
    boy = Boy()
    big_balls = [BigBall() for i in range(10)]
    balls = [Ball() for i in range(10)]
    balls = big_balls + balls
    grass = Grass()

def collide(a,b):
    left_a, bottom_a, right_a, top_a = a.get_bb()
    left_b, bottom_b, right_b, top_b = b.get_bb()

    if left_a > right_b : return False
    if right_a < left_b : return False
    if top_a < bottom_b : return False
    if bottom_a > top_b : return False

    return True

def draw(frame_time):
    clear_canvas()
    grass.draw()
    boy.draw()
    for ball in balls:
        ball.draw()

    grass.draw_bb()
    boy.draw_bb()
    for ball in balls:
        ball.draw_bb()

    update_canvas()

def main():
    open_canvas()

    create_world()

    global running
    running = True

    while running:
        global start_time
        global frame_time

        frame_time = get_time() - start_time

        #Game Logic
        handle_events()
        boy.update( frame_time)
        for ball in balls:
            ball.update(frame_time)

        for ball in balls:
            if collide(boy, ball):
                balls.remove(ball)
                boy.eat(ball)

            for ball in big_balls:
                if collide(grass, ball):
                    ball.stop()

        # GameRendering
        draw(frame_time)

        start_time += frame_time

        delay(0.05)
    close_canvas()

if __name__ == '__main__':
    main()
