from pico2d import *

import title_state
import game_framework
import random
import ai_boy_IQ10
import json_player
import numbers

grass = None
team = None
selectIndex = 0

class Grass:
    def __init__(self):
        self.image = load_image('../LabData/grass.png')
    def draw(self):
        self.image.draw(400,30)

def enter():
    global grass
    global team
    team = ai_boy_IQ10.Boy().create_team()
    grass = Grass()

def exit():
    global grass
    global team
    del(team)
    del(grass)

def handle_events():
    global selectIndex
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            game_framework.change_state(title_state)
        elif event.type == SDL_KEYDOWN and (event.key == SDLK_UP or event.key == SDLK_DOWN):
            for boy in team:
                boy.select = False
            if event.key == SDLK_UP:
                selectIndex += 1
                if selectIndex == 5:
                    selectIndex = 0
                team[selectIndex].select = True
            elif event.key == SDLK_DOWN:
                selectIndex -= 1
                if selectIndex == -1:
                    selectIndex = 5 - 1
                team[selectIndex].select = True
        else:
            team[selectIndex].handle_event(event)

def update():
        for boy in team:
            boy.update()

        delay(0.001)

def draw():
    clear_canvas()
    grass.draw()
    for boy in team:
        boy.draw()
    numbers.draw(selectIndex + 1, 740, 540)
    update_canvas()