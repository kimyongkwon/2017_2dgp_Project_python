from pico2d import *

ui_objects = []

class Button:
    font = None
    def __init__(self, imagefilename, hoverfilename = None, x=0, y=0):
        self.image = load_image(imagefilename)
        if(hoverfilename == None):
            self.hoverImage = None
        else:
            self.hoverImage = load_image(hoverfilename)
        self.x, self.y = x,y
        self.hover = False
        self.text = ""
        if (Button.font == None):
            Button.font = load_font('ConsolaMalgun.ttf')

    def draw(self):
        image = self.hoverImage if self.hover else self.image
        image.draw(self.x, self.y)
        if (self.text != ""):
            w = len(self.text) * 10
            h = 30
            x = self.x - w / 2
            y = self.y - h / 2
            #TTF_sizeUTF8(self.text, w , h)
            self.font.draw(x, y, self.text)

    def update(self):
        pass

    def handle_event(self, event):
        if event.type == SDL_MOUSEMOTION:
            if (self.hoverImage != None):
                self.hover = self.ptInRect(event.x, 600 - event.y)

        if event.type == SDL_MOUSEBUTTONDOWN:
            if self.ptInRect(event.x, 600 - event.y):
                self.onOver()
            #else:
            #    print("outOver")

    def ptInRect(self, x,y):
        if (x < self.x - self.image.w / 2):
            return False
        if (x > self.x + self.image.w / 2):
            return False
        if (y < self.y - self.image.h / 2):
            return False
        if (y > self.y + self.image.h / 2):
            return False
        return True

    def onOver(self):
        print("OnClick")

class NinePatchButton(Button):
    def __init__(self, imageFilename, w = 0, h = 0):
        self.image = load_image(imageFilename)
        self.x, self.y = x, y
        self.hover = False
        self.text = " "
    def draw(self):
        w = self.image.w
        h = self.image.h / 4
        corner = 32
        sy = h if self.hover else 0

def add(obj):
    ui_objects.append(obj)

def handle_event(event):
    for obj in ui_objects:
        obj.handle_event(event)

def draw():
    for obj in ui_objects:
        obj.draw()