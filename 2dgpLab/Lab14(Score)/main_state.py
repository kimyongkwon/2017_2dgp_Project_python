from pico2d import *


import game_framework
import random
import adjust_run_and_action
import title_state, start_state, score_state
#import json_player
import numbers
import time

start_time = 0
frame_time = 0

grass = None
team = None
selectIndex = 0

class Grass:
    def __init__(self):
        self.image = load_image('../LabData/grass.png')
    def draw(self):
        self.image.draw(400,30)

def enter():
    global grass
    global team
    team = adjust_run_and_action.Boy().create_team()
    grass = Grass()

def exit():
    global grass
    global team
    del(team)
    del(grass)

def handle_events():
    global selectIndex
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            game_framework.change_state(title_state)
        elif event.type == SDL_KEYDOWN and (event.key == SDLK_UP or event.key == SDLK_DOWN):
            for boy in team:
                boy.select = False
            if event.key == SDLK_UP:
                selectIndex += 1
                if selectIndex == 5:
                    selectIndex = 0
                team[selectIndex].select = True
            elif event.key == SDLK_DOWN:
                selectIndex -= 1
                if selectIndex == -1:
                    selectIndex = 5 - 1
                team[selectIndex].select = True
        elif event.type == SDL_MOUSEBUTTONDOWN:
            x = event.x
            print(x)
            entry = score_state.Entry(x, time.time())
            score_state.add(entry)
            game_framework.push_state(score_state)
        else:
            team[selectIndex].handle_event(event)

def update():
    global start_time
    global frame_time

    frame_time = get_time() - start_time

    for boy in team:
        boy.update(frame_time)

    start_time += frame_time

    delay(0.05)

def draw():
    clear_canvas()
    grass.draw()
    for boy in team:
        boy.draw()
    numbers.draw(selectIndex + 1, 740, 540)
    update_canvas()

def pause() : pass

def resume() : pass