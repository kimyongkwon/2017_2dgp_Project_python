
from pico2d import *

import json
import pickle
import game_framework
import main_state

name = "ScoreState"

userPickle = True

scores = []
font = None
#loadScores()

if (userPickle == True):
    fileName = "score.pickle."
else:
    fileNmae = "score.json"

class Entry:
    def __init__(self, score, time):
        self.score = score
        self.time = time

def add(score):
    global scores
    print("Now scores has " + str(len(scores)) + " entries.")
    scores.append(score)
    print("Now scores has " + str(len(scores)) + " entries.")
    saveScores()

def loadScores():
    global scores
    scores = []
    if (userPickle):
        f = open("score.pickle.txt", "rb")
        scores = pickle.load(f)
        f.close()

def saveScores():
    global scores
    if (userPickle):
        f = open("score.pickle.txt", "wb")
        pickle.dump(scores, f)
        f.close()

def enter():
    open_canvas()

def exit():
    close_canvas()

def update():
    pass

def draw():
    global font
    global scores
    if (font == None):
        font = load_font('../LabData/ConsolaMalgun.ttf')
    scores[0].draw(100,100)
    clear_canvas()
    update_canvas()

def handle_events():
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN:
            game_framework.pop_state()

def pause() : pass

def resume() : pass