from pico2d import *

import main_state, score_state
import game_framework
import time

name = 'main_State'
image = None

def enter():
    global image
    image = load_image('../LabData/title.png')

def exit():
    global image
    del (image)

def handle_events():
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif (event.type == SDL_MOUSEBUTTONDOWN):
            x = event.x
            print(x)
            entry = score_state.Entry(x, time.time())
            score_state.add(entry)
            game_framework.push_state(score_state)

def draw():
    clear_canvas()
    image.draw(400, 300)
    update_canvas()

def update() : pass

def pause() : pass

def resume() : pass
