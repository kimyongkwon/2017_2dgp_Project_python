from pico2d import*

open_canvas()

grass = load_image('../LabData/grass.png')
character = load_image('../LabData/character.png')

grass.draw_now(400, 30)
character.draw_now(400, 90)

delay(3)

close_canvas()
