'''
from pico2d import *
open_canvas()

grass = load_image('../LabData/grass.png')
character = load_image('../LabData/run_animation.png')

x = 0
frame = 0

while ( x < 800 ):
    clear_canvas()
    grass.draw(400,30)
    character.clip_draw(frame * 100, 0, 100 , 100, x, 90)
    update_canvas()
    frame = (frame + 1) % 8
    delay(0.05)
    x += 5
    get_events()
close_canvas()
'''

from pico2d import *

open_canvas()

grass = load_image('../LabData/grass.png')
character = load_image('../LabData/Bomb_Animation1.png')

x = 0
frame = 0
time = 0

while (x < 800):
    clear_canvas()
    grass.draw(400, 30)
    character.clip_draw(frame * 128, 128 * (frame % 4), 100, 100, x, 90)

    update_canvas()

    print(frame)
    if (time < 2):
        time += 1
        if (time >= 2):
            frame = (frame + 1) % 27
            time = 0

    delay(0.01)
    x += 5

    get_events()

close_canvas()