from pico2d import *

import boy
import ball

start_time = 0
frame_time = 0

class Grass:
    def __init__(self):
        self.x = 400
        self.y = 30
        self.image = load_image('../LabData/grass.png')
    def draw(self):
        self.image.draw(self.x, self.y)
    def get_bb(self):
        return self.x - 400, self.y - 30, self.x + 400, self.y + 30
    def draw_bb(self):
        draw_rectangle(*self.get_bb())

def handle_events():
    global running
    global x
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            running = False
        elif event.type == SDL_MOUSEMOTION:
            x, y = event.x, 600 - event.y
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            running = False
        boy.handle_event(event)

def create_world():
    global boy, grass, balls, big_balls
    boy = boy.Boy()
    big_balls = [ball.BigBall() for i in range(10)]
    balls = [ball.Ball() for i in range(10)]
    balls = big_balls + balls
    grass = Grass()

def collide(a,b):
    left_a, bottom_a, right_a, top_a = a.get_bb()
    left_b, bottom_b, right_b, top_b = b.get_bb()

    if left_a > right_b : return False
    if right_a < left_b : return False
    if top_a < bottom_b : return False
    if bottom_a > top_b : return False

    return True

def draw(frame_time):
    clear_canvas()
    grass.draw()
    boy.draw()
    for ball in balls:
        ball.draw()

    grass.draw_bb()
    boy.draw_bb()
    for ball in balls:
        ball.draw_bb()

    update_canvas()

def main():
    open_canvas()

    create_world()

    global running
    running = True

    while running:
        global start_time
        global frame_time

        frame_time = get_time() - start_time

        #Game Logic
        handle_events()
        boy.update( frame_time)
        for ball in balls:
            ball.update(frame_time)

        for ball in balls:
            if collide(boy, ball):
                balls.remove(ball)

            for ball in big_balls:
                if collide(grass, ball):
                    ball.stop()

        # GameRendering
        draw(frame_time)

        start_time += frame_time

        delay(0.05)
    close_canvas()

if __name__ == '__main__':
    main()
