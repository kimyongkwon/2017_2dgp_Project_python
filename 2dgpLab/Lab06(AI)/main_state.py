from pico2d import *

import title_state
import game_framework
import random
import ai_boy
import ai_boy_IQ10

grass = None
team = None

class Grass:
    def __init__(self):
        self.image = load_image('../LabData/grass.png')
    def draw(self):
        self.image.draw(400,30)

def enter():
    global grass
    global team
    team = [ai_boy_IQ10.Boy() for i in range(10)]
    grass = Grass()

def exit():
    global grass
    global team
    del(team)
    del(grass)

def handle_events():
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            game_framework.change_state(title_state)

def update():
        for boy in team:
            boy.update()
        delay(0.05)

def draw():
    clear_canvas()
    grass.draw()
    for boy in team:
        boy.draw()
    update_canvas()