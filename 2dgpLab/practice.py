from pico2d import *

class A:
    def __init__(self):
        print("__init__")
        self. d = 10

    def __del__(self):
        print("__del__")
        del self.d



a = A()

print(a.d)

a.d = None

print(a.d)


